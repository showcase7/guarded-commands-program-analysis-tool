{
  int a;
  int b;
  {int fst; int snd} R;
  int[10] A;
  a := 2;
  b := 3;
  
  a := 1 + true;
  b := false / 2;
  a := true % 3;
  b := 4 * R;
  a := A - 5;
  
  if (6 < false) {}
  if (true >= 7) {}
  if (true <= false) {}
  if (10 < true) {}
  if (11 == false) {}
  if (12 != true) {}
  if (13 | 14) {}
  if (15 & false) {}
}
