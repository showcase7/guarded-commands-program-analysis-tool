{
  int a;
  int b;
  int[123] c;
  {int fst; int snd} R;
  int a1234;
  int ALL_CAPS;
  
  a := 2;
  b := 3;
  
  if (a < b & a <= b & b > a & b >= a) {
    while ( a == b | a != b) {
      a := a * b;
      R.fst := a + b;
      R.snd := a - b;
      c[a] := 6 % b;
      c[20] := 12 / a;
      R := (a, b);
    }
  } else {
    while ( true | not false) {
      read a;
      write a + 2;
      R.fst := c[13];
    }
  }
}
