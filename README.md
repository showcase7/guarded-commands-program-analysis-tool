# Guarded commands program analysis tool
## Description
This program can be used to run common program analyses on programs written in Edsger Dijkstra's 'guarded command language'. The program can also compile these programs into bytecode and execute them.

The supported analyses are:
- Faint variables
- Live variables
- Reaching definitions
- Sign detection

The program was a project I did as part of a graduate course on project analysis by Nielson and Nielson. 

## Building the program
Use the Rust package manager `cargo` to build it, using `cargo build --release`. This builds the executable as `target/release/microcc`.

## Running the program
The program can be run by `target/release/microcc --help` or through cargo as `cargo run --release -- --help`.

An example invocation on one of the test programs can be done with `cargo run --release analyse rd stk samples/example.mc`. This runs the 'reaching definitions' anlysis using the 'stack' work-list (fixed-point propagation method) on the file named 'example.mc'.

## License
This project is available under the Universal Permissive License, Version 1.0 ([LICENSE.txt]) or https://oss.oracle.com/licenses/upl/ .
