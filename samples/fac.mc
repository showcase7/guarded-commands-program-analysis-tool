{
  int x;
  int y;
  
  read x;
  y := 1;
  
  while (x > 0) {
    y := y * x;
    x := x - 1;
  }
  
  write y;
}
