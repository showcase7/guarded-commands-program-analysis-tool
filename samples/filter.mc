// filter.mc
// The poor man's filter function
{
    int limit;
    int n;
    int i;
    int x;
    
    read limit;
    read n;
    
    while (i < n) {
        read x;
        if (x > limit) {
            write x;
        }
        i := i + 1;
    }
}
