//! Module to format untyped programs prettily.

use ast::*;

/// Formats an expression into the given string.
pub fn format_expression_into(expr: &Expr, s: &mut String) {
    use ast::ExprKind::*;
    use ast::BinOp::*;
    match expr.kind {
        Int(value) => {
            s.push_str(&value.to_string());
        }
        Access(ref loc) => {
            format_location_into(loc, s);
        }
        Bool(value) => {
            s.push_str(&value.to_string());
        }
        Binary(ref left, op, ref right) => {
            format_expression_into(left, s);
            s.push(' ');
            s.push_str(match op {
                Plus => "+",
                Minus => "-",
                Times => "*",
                Div => "/",
                Rem => "%",
                // ...
                Lt => "<",
                LtEq => "<=",
                Gt => ">",
                GtEq => ">=",
                Eq => "==",
                NotEq => "!=",
                // ...
                And => "&",
                Or => "|",
            });
            s.push(' ');
            format_expression_into(right, s);
        }
        Unary(op, ref expr) => {
            s.push_str(match op {
                UnOp::Not => "not",
            });
            s.push(' ');
            format_expression_into(expr, s);
        }
        Record(ref fst, ref snd) => {
            s.push('(');
            format_expression_into(fst, s);
            s.push_str(", ");
            format_expression_into(snd, s);
            s.push(')');
        }
    }
}

/// Formats a location expression into the given string.
pub fn format_location_into(loc: &Loc, s: &mut String) {
    use ast::LocKind::*;
    match loc.kind {
        Var => {
            s.push_str(&loc.name.text);
        }
        Arr(ref expr) => {
            s.push_str(&loc.name.text);
            s.push('[');
            format_expression_into(expr, s);
            s.push(']');
        }
        Field(ref ident) => {
            s.push_str(&loc.name.text);
            s.push('.');
            s.push_str(&ident.text);
        }
    }
}

/// Formats a statements into the given string.
pub fn format_statement_into(statement: &Stmt, s: &mut String, indent: usize) {
    use ast::StmtKind::*;
    
    match statement.kind {
        Assign(ref loc, ref expr) => {
            format_location_into(loc, s);
            s.push_str(" := ");
            format_expression_into(expr, s);
            s.push(';');
        },
        If(ref expr, ref body, ref else_body) => {
            s.push_str("if (");
            format_expression_into(expr, s);
            s.push_str(") {\n");
            let spaces = (indent + 1) * 4;
            for stmt in body {
                for _ in 0..spaces {
                    s.push(' ');
                }
                format_statement_into(stmt, s, indent + 1);
                s.push('\n');
            }

            for _  in 0 .. indent * 4 {
                s.push(' ');
            }
            s.push('}');
            if let Some(else_body) = else_body.as_ref() {
                s.push_str(" else {\n");
                let spaces = (indent + 1) * 4;
                for stmt in else_body {
                    for _ in 0..spaces {
                        s.push(' ');
                    }
                    format_statement_into(stmt, s, indent + 1);
                    s.push('\n');
                }
                for _  in 0 .. indent * 4 {
                    s.push(' ');
                }
                s.push('}');
            }
        },
        While(ref expr, ref body) => {
            s.push_str("while (");
            format_expression_into(expr, s);
            s.push_str(") {\n");
            let spaces = (indent + 1) * 4;
            for stmt in body {
                for _ in 0..spaces {
                    s.push(' ');
                }
                format_statement_into(stmt, s, indent + 1);
                s.push('\n');
            }
            for _  in 0 .. indent * 4 {
                s.push(' ');
            }
            s.push('}');
        },
        Break => {
            s.push_str("break;")
        }
        Read(ref loc) => {
            s.push_str("read ");
            format_location_into(loc, s);
            s.push(';');
        },
        Write(ref expr) => {
            s.push_str("write ");
            format_expression_into(expr, s);
            s.push(';');
        },
    }
}

pub fn format_declaration_into(dec: &Dec, s: &mut String) {
    use ast::DecKind::*;
    
    match dec.kind {
        Int => {
            s.push_str("int ");
            s.push_str(&dec.name.text);
            s.push_str(";");
        }
        IntArr(size) => {
            s.push_str("int[");
            s.push_str(&(size.to_string()));
            s.push_str("] ");
            s.push_str(&dec.name.text);
            s.push_str(";");
        }
        Record => {
            s.push_str("{{int fst; int snd}} ");
            s.push_str(&dec.name.text);
            s.push_str(";");
        }
    }
}

/// Formats a program prettily into the given string.
pub fn format_program_into(program: &Program, s: &mut String) {
    s.push_str("{\n");
    let indent = 1;
    
    for dec in &program.declarations {
        for _ in 0 .. indent*4 {
            s.push(' ');
        }
        format_declaration_into(dec, s);
        s.push('\n');
    }
    
    if ! program.declarations.is_empty() && ! program.statements.is_empty() {
        s.push('\n');
    }
    
    for stmt in &program.statements {
        for _ in 0 .. indent*4 {
            s.push(' ');
        }
        format_statement_into(stmt, s, indent);
        s.push('\n');
    }
    s.push_str("}\n");
}
