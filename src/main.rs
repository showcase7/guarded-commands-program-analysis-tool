extern crate microcc;

use microcc::{Lexer, TokenKind, Instr, Prograph};
use microcc::analysis::{Analysis, VertexId, QueueWl, StackWl, RevPostWl, RoundRobinWl};
use microcc::analysis::{ReachingDefinitions, RdState};
use microcc::analysis::{LiveVariables, LvState};
use microcc::analysis::{FaintVariables, FvState};
use microcc::analysis::{SignDetection, SdState};

use microcc::lexer::{RawLexer};
use microcc::ast::{Program, TProgram};

use std::fs::File;
use std::error::Error;
use std::env;
use std::io::{Read, Write};
use std::process::Command;
use std::path::PathBuf;

// Prints the filtered tokens of a source file in a semi-formatted way.
fn _print_filtered_tokens(source: &str) {
    println!("Filtered tokens:");
    println!("");
    let lexer = Lexer::new(source);
    let mut indent = String::new();
    for res in lexer {
        match res {
            Ok(token) => {
                let text = &source[token.span.start .. token.span.end];
                match token.kind {
                    TokenKind::Ident => {
                        print!("'{}' ", text);
                    }
                    TokenKind::Semicolon => {
                        println!("{}", text);
                        print!("{}", indent);
                    }
                    TokenKind::CurlyOpen => {
                        println!("{}", text);
                        indent.push(' ');
                        indent.push(' ');
                        print!("{}", indent);
                    }
                    TokenKind::CurlyClose => {
                        indent.pop();
                        indent.pop();
                        //println!("");
                        println!("{}", text);
                        print!("{}", indent);
                    }
                    TokenKind::Eos => {
                        println!("<End of stream>");
                    }
                    _ => {
                        print!("{} ", text);
                    }
                }
            }
            Err(err) => {
                println!("");
                println!("// error:{}:{}: {}", err.line, err.col, err.message)
            }
        }
    }
}

fn _print_raw_ast(program: &Program) {
    println!("Raw AST:");
    println!("{:#?}", program);
}

fn print_usage(cmd: Option<&str>) -> Result<(), ErrCode> {
    println!("Usage: microcc [-h | --help] {} <file>", cmd.unwrap_or("<command>"));
    Err(ErrCode::WrongUsage)
}

fn print_help() {
    let _ = print_usage(None);
    println!("");
    println!("Commands:");
    println!("  lex:");
    println!("    Lexes the given file and prints the found tokens.");
    println!("");
    println!("  ast:");
    println!("    Parses the given file and prints the resulting abstract syntax tree.");
    println!("");
    println!("  typeck:");
    println!("    Type-checks the given file and prints any errors found.");
    println!("");
    println!("  codegen:");
    println!("    Generates MicroC VM instructions for the given file and prints them.");
    println!("");
    println!("  run:");
    println!("    Compiles and runs the program in the given file.");
    println!("");
    println!("  analyse:");
    println!("    Analyses the program in the given file using a chosen analysis");
    println!("    and work list.");
    println!("");
    println!("    Available analyses:");
    println!("      rd: 'Reaching Definitions'");
    println!("      lv: 'Live Variables'");
    println!("      fv: 'Faint Variables'");
    println!("      sd: 'Sign Detection'");
    println!("");
    println!("    Available work lists (graph traversal orders):");
    println!("      stk: Stack-based traversal (LIFO)");
    println!("      que: Queue-based traversal (FIFO)");
    println!("      rep: Reverse post-order traversal");
    println!("      rr:  Round-Robin-based post-order traversal");
    println!("");
    println!("  fmt:");
    println!("    Parses the given file and formats it prettily to stdout.");
    println!("");
    println!("  dot:");
    println!("    Outputs a DOT graph description of the program.");
    println!("");
    println!("  graph: (Requires the graphviz 'dot' binary)");
    println!("    Creates a png of the program graph using graphviz and stores");
    println!("    it at 'microcc_program_graph.png'");
}

/// Loads the given file to a string, printing errors on failure.
fn load_file(path: &str) -> Result<String, ErrCode> {
    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(err) => {
            eprintln!("Could not open file at '{}': {}", path, err.description());
            return Err(ErrCode::CouldNotOpenFile);
        }
    };
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Ok(_) => {}
        Err(err) => {
            eprintln!("Could not read file at '{}': {}", path, err.description());
            return Err(ErrCode::CouldNotReadFile);
        }
    }
    Ok(s)
}

#[derive(Debug)]
#[repr(i32)]
pub enum ErrCode {
    WrongUsage = 1,
    CouldNotOpenFile = 2,
    CouldNotReadFile = 3,
    ParseFailed = 4,
    HadLexingErrors = 5,
    SyntaxAndSemanticErrors = 6,
    SemanticErrors = 7,
    CouldNotCreateFile = 8,
    CouldNotWriteFile = 9,
    CouldNotRunDot = 10,
    VMError = 11,
}

/// Runs the parser on the source file, printing error messages on error.
fn parse_verbose(source: &str) -> Result<Program, ErrCode> {
    match microcc::parse(source) {
        Ok(program) => {
            Ok(program)
        }
        Err((_, errors)) => {
            eprintln!("Parse failed!");
            eprintln!("Errors:");
            for error in errors {
                eprintln!("  {}:{}: {}", error.line, error.col, error.message);
            }
            Err(ErrCode::ParseFailed)
        }
    }
}

/// Runs the type-checker on the given program, printing error messages
/// on error.
fn type_check_verbose(program: &Program) -> Result<TProgram, ErrCode> {
    match microcc::type_check(&program) {
        Ok(typed_program) => {
            Ok(typed_program)
        }
        Err(errors) => {
            eprintln!("Semantic errors:");
            for error in errors {
                eprintln!("  {:#?}", error);
            }
            Err(ErrCode::SemanticErrors)
        }
    }
}

/// Runs the parser and type-checker on the given source code, printing error
/// messages on error.
fn parse_and_type_check_verbose(source: &str) -> Result<TProgram, ErrCode> {
    match microcc::parse(source) {
        Ok(program) => {
            type_check_verbose(&program)
        }
        Err((program, errors)) => {
            eprintln!("Parse failed!");
            eprintln!("Syntax errors:");
            for error in errors {
                eprintln!("  {}:{}: {}", error.line, error.col, error.message);
            }
            match microcc::type_check(&program) {
                Ok(_) => {
                    Err(ErrCode::SemanticErrors)
                }
                Err(errors) => {
                    eprintln!("");
                    eprintln!("Semantic errors:");
                    for error in errors {
                        eprintln!("  {:#?}", error);
                    }
                    Err(ErrCode::SyntaxAndSemanticErrors)
                }
            }
        }
    }
}

fn cmd_graph(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2  {
        return print_usage(Some("graph"));
    }
    let source = load_file(&args[1])?;
    let program = parse_verbose(&source)?;
    let typed_program = type_check_verbose(&program)?;
    let graph = Prograph::new(&typed_program);
    let s = graph.to_dot();
    
    const FILENAME: &'static str = "microcc_program_graph.dot";
    let mut out_path = PathBuf::from(&args[1]);
    out_path.set_extension("png");
    match File::create(FILENAME) {
        Ok(mut file) => {
            match file.write_all(s.as_bytes()) {
                Ok(_) => {}
                Err(err) => {
                    eprintln!("Could not write DOT file to '{}': {}", FILENAME, err.description());
                    return Err(ErrCode::CouldNotWriteFile);
                }
            }
        },
        Err(err) => {
            eprintln!("Could not create DOT file at '{}': {}", FILENAME, err.description());
            return Err(ErrCode::CouldNotCreateFile);
        }
    }

    match Command::new("dot")
        .arg("-Tpng")
        .arg("-o").arg(out_path.file_name().unwrap())
        .arg(FILENAME).output() 
    {
        Ok(_) => {
            println!("Graph written to '{}'", out_path.file_name().unwrap().to_string_lossy());
        }
        Err(_) => {
            eprintln!("Could not run 'dot' (is graphviz installed?)");
            return Err(ErrCode::CouldNotRunDot);
        }
    }

    match Command::new("open").arg(out_path.file_name().unwrap()).output() {
        Ok(_) => {}
        Err(_) => {
            //eprintln!("Could not open image '{}'", out_path.to_string_lossy());
        }
    }
    
    Ok(())
}

fn cmd_lex(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("lex"));
    }
    let source = load_file(&args[1])?;
    println!("Tokens:");
    let lexer = RawLexer::new(&source);
    let mut had_error = false;
    for res in lexer {
        match res {
            Ok(token) => {
                let text = &source[token.span.start .. token.span.end];
                let (line, col) = microcc::text_pos(token.span.start, &source);
                println!("{:3}:{:02}: {:?}: {:?} ", line, col, token.kind, text);
            }
            Err(err) => {
                eprintln!("{:3}:{:02}: ERROR: {}", err.line, err.col, err.message);
                had_error = true;
            }
        }
    }
    
    if had_error {
        Err(ErrCode::HadLexingErrors)
    } else {
        Ok(())
    }
}

fn cmd_ast(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("ast"));
    }
    let source = load_file(&args[1])?;
    let program = parse_verbose(&source)?;
    println!("{:#?}", program);
    Ok(())
}

fn cmd_typeck(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("typeck"));
    }
    let source = load_file(&args[1])?;
    match parse_and_type_check_verbose(&source) {
        Ok(_) => {
            println!("The program is completely valid :)");
            Ok(())
        }
        Err(err) => {
            Err(err)
        }
    }
}

fn cmd_dot(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("dot"));
    }
    let source = load_file(&args[1])?;
    let program = parse_verbose(&source)?;
    let typed_program = type_check_verbose(&program)?;
    let graph = Prograph::new(&typed_program);
    let s = graph.to_dot();
    println!("{}", s);
    Ok(())
}

fn cmd_fmt(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("fmt"));
    }
    let source = load_file(&args[1])?;
    let program = parse_verbose(&source)?;
    let mut s = String::new();
    microcc::format_program_into(&program, &mut s);
    println!("{}", s);
    Ok(())
}

fn cmd_codegen(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("codegen"));
    }
    let source = load_file(&args[1])?;
    let typed_program = parse_and_type_check_verbose(&source)?;
    let instructions = microcc::generate_instructions(&typed_program);
    println!("Instructions:");
    for (i, &instr) in instructions.iter().enumerate() {
        match instr {
            Instr::Skip => println!("\n     [{}]\n", i),
            _ => println!("{:3}: {:?}", i, instr),
        }
    }
    Ok(())
}

fn cmd_run(args: &[String]) -> Result<(), ErrCode> {
    if args.len() != 2 {
        return print_usage(Some("run"));
    }
    let source = load_file(&args[1])?;
    let typed_program = parse_and_type_check_verbose(&source)?;
    let instructions = microcc::generate_instructions(&typed_program);
    eprintln!("Execution started!");
    if let Some(code) = microcc::execute(&instructions) {
        eprintln!("ERROR: Exit code {}", code);
        Err(ErrCode::VMError)
    } else {
        eprintln!("Program finished running succesfully :)");
        Ok(())
    }
}

fn print_rd_result(domain: &Vec<RdState>, graph: &Prograph) {
    let end_state = 1;
    
    fn print_rd_state(vertex: usize, state: &RdState, graph: &Prograph) {
        print!("{:3}: |", vertex);
        let var_names = graph.var_names();
        for loc in graph.basic_locations() {
            if let Some(places) = state.get(loc) {
                let mut sorted = places.iter().collect::<Vec<_>>();
                sorted.sort();
                print!("{}: ", loc.format(&var_names));
                for (j, &&edge_id) in sorted.iter().enumerate() {
                    print!("{}", graph.format_edge(edge_id));
                    if j != sorted.len() - 1 {
                        print!(", ");
                    }
                }
                print!(" | ");
            }
        }
        println!("");
    }
    
    println!("Reaching Definitions:");
    for (i, state) in domain.iter().enumerate() {
        if i == end_state {
            continue;
        }
        print_rd_state(i, state, graph);
    }
    print_rd_state(end_state, &domain[end_state], graph);
}

fn print_lv_result(domain: &Vec<LvState>, graph: &Prograph) {
    let end_state = 1;
    
    fn print_lv_state(vertex: usize, state: &LvState, graph: &Prograph) {
        print!("{:3}: {{ ", vertex);
        let last = if state.len() == 0 { 0 } else { state.len() - 1 };
        let mut sorted = state.iter().collect::<Vec<_>>();
        sorted.sort();
        let var_names = graph.var_names();
        for (i, &loc) in sorted.iter().enumerate() {
            print!("{}", loc.format(var_names));
            if i != last {
                print!(", ");
            }
        }
        if ! state.is_empty() {
            print!(" ");
        }
        println!("}}");
    }
    
    println!("Live Variables:");
    for (i, state) in domain.iter().enumerate() {
        if i == end_state {
            continue;
        }
        print_lv_state(i, state, graph);
    }
    print_lv_state(end_state, &domain[end_state], graph);
}

fn print_fv_result(domain: &Vec<FvState>, graph: &Prograph) {
    let end_state = 1;
    
    fn print_fv_state(vertex: usize, state: &FvState, graph: &Prograph) {
        print!("{:3}: {{", vertex);
        let last = if state.len() == 0 { 0 } else { state.len() - 1 };
        let mut sorted = state.iter().collect::<Vec<_>>();
        sorted.sort();
        let var_names = graph.var_names();
        for (i, &loc) in sorted.iter().enumerate() {
            print!(" {}", loc.format(var_names));
            if i != last {
                print!(",");
            }
        }
        if ! state.is_empty() {
            print!(" ");
        }
        println!("}}");
    }
    
    println!("Faint Variables:");
    for (i, state) in domain.iter().enumerate() {
        if i == end_state {
            continue;
        }
        print_fv_state(i, state, graph);
    }
    print_fv_state(end_state, &domain[end_state], graph);
}

fn print_sd_result(domain: &Vec<SdState>, graph: &Prograph) {
    let end_state = 1;
    
    fn print_sd_state(vertex: usize, state: &SdState, graph: &Prograph) {
        print!("{:3}: {{", vertex);
        let last = if state.len() == 0 { 0 } else { state.len() - 1 };
        let mut sorted = state.iter().collect::<Vec<_>>();
        sorted.sort_by_key(|(&loc, _)| loc);
        let var_names = graph.var_names();
        for (i, (&loc, &signs)) in sorted.iter().enumerate() {
            print!(" {}: {}", loc.format(var_names), signs);
            if i != last {
                print!(",");
            }
        }
        if ! state.is_empty() {
            print!(" ");
        }
        println!("}}");
    }
    
    println!("Possible variable signs (very imprecise):");
    for (i, state) in domain.iter().enumerate() {
        if i == end_state {
            continue;
        }
        print_sd_state(i, state, graph);
    }
    print_sd_state(end_state, &domain[end_state], graph);
}

enum WlType<'graph> {
    Stack(StackWl<'graph>),
    Queue(QueueWl<'graph>),
    RevPost(RevPostWl<'graph>),
    RoundRobin(RoundRobinWl<'graph>),
}

enum AnType {
    ReachDefs(ReachingDefinitions),
    LiveVars(LiveVariables),
    FaintVars(FaintVariables),
    SignDet(SignDetection),
}

fn run_analysis<'graph>(analysis: AnType, mut work_list: WlType<'graph>, graph: &'graph Prograph) {
    use self::WlType::*;
    use self::AnType::*;
    
    const FIRST_STATE: VertexId = 0;
    const LAST_STATE: VertexId = 1;
    
    match analysis {
        ReachDefs(mut analysis) => {
            let res = match work_list {
                Stack(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                Queue(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                RevPost(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                RoundRobin(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
            };
            print_rd_result(&res, graph);
        }
        LiveVars(mut analysis) => {
            let res = match work_list {
                Stack(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                Queue(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                RevPost(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                RoundRobin(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
            };
            print_lv_result(&res, graph);
        }
        FaintVars(mut analysis) => {
            let res = match work_list {
                Stack(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                Queue(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                RevPost(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
                RoundRobin(ref mut wl) => analysis.run(graph, wl, LAST_STATE, |_, _| {}),
            };
            print_fv_result(&res, graph);
        }
        SignDet(mut analysis) => {
            let res = match work_list {
                Stack(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                Queue(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                RevPost(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
                RoundRobin(ref mut wl) => analysis.run(graph, wl, FIRST_STATE, |_, _| {}),
            };
            print_sd_result(&res, graph);
            println!("Finished detecting signs!");
        }
    }
}

fn cmd_analyse(args: &[String]) -> Result<(), ErrCode> {
    const USAGE_STR: &'static str = "analyse [rd|lv|fv|sd] [stk|que|rep|rr]";
    if args.len() != 4 {
        return print_usage(Some(USAGE_STR));
    }
    
    let analysis = match (&args[1]).as_str() {
        "rd" => AnType::ReachDefs(ReachingDefinitions::new()),
        "lv" => AnType::LiveVars(LiveVariables::new(10)),
        "fv" => AnType::FaintVars(FaintVariables::new(10)),
        "sd" => AnType::SignDet(SignDetection::new()),
        other => {
            eprintln!("Invalid analysis type: '{}'", other);
            return print_usage(Some(USAGE_STR));
        }
    };
    
    let work_list = match (&args[2]).as_str() {
        //"rnd" => 
        "stk" => WlType::Stack(StackWl::new()),
        "que" => WlType::Queue(QueueWl::new()),
        "rep" => WlType::RevPost(RevPostWl::new()),
        "rr" => WlType::RoundRobin(RoundRobinWl::new()),
        other => {
            eprintln!("Invalid work list type: '{}'", other);
            return print_usage(Some(USAGE_STR));
        }
    };
        
    let source = load_file(&args[3])?;
    let typed_program = parse_and_type_check_verbose(&source)?;
    let graph = Prograph::new(&typed_program);
    
    run_analysis(analysis, work_list, &graph);
    Ok(())
}

fn main() -> Result<(), ErrCode> {
    
    let args = env::args().skip(1).collect::<Vec<String>>();
    
    if args.len() == 0 {
        return print_usage(None);
    }
    
    match args[0].as_str() {
        "lex"       => cmd_lex(&args),
        "ast"       => cmd_ast(&args),
        "typeck"    => cmd_typeck(&args),
        "dot"       => cmd_dot(&args),
        "graph"     => cmd_graph(&args),
        "fmt"       => cmd_fmt(&args),
        "codegen"   => cmd_codegen(&args),
        "run"       => cmd_run(&args),
        "analyse"   => cmd_analyse(&args),
        "help"   | 
        "-h"     | 
        "--help" => {
            print_help();
            Ok(())
        }
        other => {
            eprintln!("Unrecognized command '{}'", other);
            print_usage(None)
        }
    }
}


