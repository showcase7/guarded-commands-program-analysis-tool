
const ALL_TOKENS: &'static str = include_str!("../tests/lex_all_tokens.mc");
const DOUBLE_DEC: &'static str = include_str!("../tests/tc_double_dec.mc");
const NO_STATEMENTS: &'static str = include_str!("../tests/tc_no_statements.mc");
const UNBOUND_VARS: &'static str = include_str!("../tests/tc_unbound_vars.mc");
const BAD_READS: &'static str = include_str!("../tests/tc_bad_reads.mc");
const BAD_BINOPS: &'static str = include_str!("../tests/tc_bad_binops.mc");
const COND_IS_BOOL: &'static str = include_str!("../tests/tc_cond_is_bool.mc");


#[cfg(test)]
mod tests {
    
    mod lexer {
        use test::*;
        use lexer::Lexer;
        
        
        fn run_lexer_test(source: &str) -> bool {
            let lexer = Lexer::new(source);
            let mut had_error = false;
            println!("Testing lexer...");
            for res in lexer {
                match res {
                    Ok(token) => {}
                    Err(err) => {
                        println!("  error:{}:{}: {}", err.line, err.col, err.message);
                        had_error = true;
                    }
                }
            }
            had_error
        }
        
        #[test]
        // Test that the lexer accepts all the tokens it should
        fn all_tokens() {
            let had_error = run_lexer_test(ALL_TOKENS);
            assert!(had_error == false);
        }
        
        #[test]
        // Test the lexer on the other test scripts.
        fn other_test_cases() {
            let mut had_error = false;
            for test_source in &[DOUBLE_DEC, NO_STATEMENTS, UNBOUND_VARS, BAD_READS, BAD_BINOPS, COND_IS_BOOL] {
                let res = run_lexer_test(test_source);
                had_error = had_error || res;
            }
            
            assert!(had_error == false);
        }
    }
    
    mod parser {
    }
    
    mod typeck {
        use test::*;
        use ast::*;
        use span::Span;
        use parser::Parser;
        use typeck::{type_check, TypeError};
        
        fn run_type_checker(source: &str) -> Result<TProgram, Vec<TypeError>> {
            let parser = Parser::new(source);
            match parser.parse() {
                Ok(program) => type_check(&program),
                Err((program, parse_errors)) => {
                    eprintln!("Syntax errors:");
                    for error in parse_errors {
                        eprintln!("  {}:{}: {}", error.line, error.col, error.message);
                    }
                    eprintln!("");
                    panic!("Syntax errors in type-checking test");
                },
            }
        }
        
        #[test]
        fn other_test_cases() {
            let mut had_error = false;
            for test_source in &[ALL_TOKENS] {
                let parser = Parser::new(test_source);
                match parser.parse() {
                    Ok(program) => {
                        match type_check(&program) {
                            Ok(_) => {}
                            Err(errors) => {
                                eprintln!("'{}': Semantic errors:", test_source);
                                for error in errors {
                                    eprintln!("  {:#?}", error);
                                }
                                eprintln!("");
                                had_error = true;
                            }
                        }
                    },
                    Err((_, parse_errors)) => {
                        eprintln!("'{}': Syntax errors:", test_source);
                        for error in parse_errors {
                            eprintln!("  {}:{}: {}", error.line, error.col, error.message);
                        }
                        eprintln!("");
                        had_error = true;
                    },
                }
            }
            assert!(!had_error, "The other test cases had semantic errors!");
        }
        
        /*#[test]
        fn double_declaration() {
            match run_type_checker(DOUBLE_DEC) {
                Ok(_) => {
                    panic!("Type-check did not catch double declaration of variable");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::NameBoundTwice { 
                            first: Ident { text: "a".to_string(), span: Span { start: 8, end: 9 } }, 
                            second: Ident { text: "a".to_string(), span: Span { start: 20, end: 21 } } 
                        }
                    ]);
                }
            }
        }*/
        
        #[test]
        fn no_statements() {
            match run_type_checker(NO_STATEMENTS) {
                Ok(_) => {
                    panic!("Type-check did not catch program with no statements");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::NoStatementsInProgram
                    ]);
                }
            }
        }
        
        #[test]
        fn unbound_variables() {
            match run_type_checker(UNBOUND_VARS) {
                Ok(_) => {
                    panic!("Type-check did not catch program with unbound variables");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::UnboundVariable(
                            Ident { text: "a".to_string(), span: Span { start: 4, end: 5 } }
                        ), 
                        TypeError::UnboundVariable(
                            Ident { text: "b".to_string(), span: Span { start: 21, end: 22 } }
                        ),
                        TypeError::UnboundVariable(
                            Ident { text: "c".to_string(), span: Span { start: 31, end: 32 } }
                        ), 
                        TypeError::UnboundVariable(
                            Ident { text: "d".to_string(), span: Span { start: 45, end: 46 } }
                        )
                    ]);
                }
            }
        }
        
        #[test]
        fn bad_reads() {
            match run_type_checker(BAD_READS) {
                Ok(_) => {
                    panic!("Type-check did not catch program with bad read locations");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::ReadLocationNotInteger(TLoc { 
                            var: Var { 
                                id: 0, 
                                span: Span { start: 57, end: 58 }
                            }, 
                            kind: TLocKind::Var, 
                            span: Span { start: 57, end: 58 }, 
                            typ: Type::IntArr(4) 
                        }), 
                        TypeError::ReadLocationNotInteger(TLoc { 
                            var: Var { 
                                id: 1, 
                                span: Span { start: 67, end: 68 } 
                            }, 
                            kind: TLocKind::Var, 
                            span: Span { start: 67, end: 68 }, 
                            typ: Type::Record 
                        })
                    ]);
                }
            }
        }
        
        #[test]
        fn cond_is_bool() {
            match run_type_checker(COND_IS_BOOL) {
                Ok(_) => {
                    panic!("Type-check did not catch program with bad condition types");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::NonBooleanCondition(Expr { 
                            kind: ExprKind::Int(2), span: Span { start: 36, end: 37 } 
                        }), 
                        TypeError::NonBooleanCondition(Expr { 
                            kind: ExprKind::Access(Loc { 
                                name: Ident { text: "A".to_string(), span: Span { start: 50, end: 51 } }, 
                                kind: LocKind::Arr(Box::new(Expr { kind: ExprKind::Int(2), span: Span { start: 52, end: 53 } })), 
                                span: Span { start: 51, end: 54 } 
                            }), 
                            span: Span { start: 51, end: 54 } 
                        }), 
                        TypeError::NonBooleanCondition(Expr { 
                            kind: ExprKind::Access(Loc { 
                                name: Ident { text: "a".to_string(), span: Span { start: 65, end: 66 } }, 
                                kind: LocKind::Var, span: Span { start: 65, end: 66 } 
                            }), 
                            span: Span { start: 65, end: 66 } 
                        }), 
                        TypeError::NonBooleanCondition(Expr { 
                            kind: ExprKind::Binary(
                                Box::new(Expr { kind: ExprKind::Int(2), span: Span { start: 77, end: 78 } }), 
                                BinOp::Plus, 
                                Box::new(Expr { kind: ExprKind::Int(2), span: Span { start: 81, end: 82 } }) 
                            ), 
                            span: Span { start: 77, end: 82 } 
                        })
                    ]);
                }
            }
        }
        
        #[test]
        fn binary_operators() {
            match run_type_checker(BAD_BINOPS) {
                Ok(_) => {
                    panic!("Type-check did not catch program with bad binop arguments");
                }
                Err(errors) => {
                    assert_eq!(errors, vec![
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 91, end: 95 } }, 
                            op: BinOp::Plus, expected: Type::Int, full_span: Span { start: 87, end: 95 } 
                        }, 
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Bool(false), typ: Type::Bool, span: Span { start: 104, end: 109 } }, 
                            op: BinOp::Div, expected: Type::Int, full_span: Span { start: 104, end: 113 } 
                        }, 
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 122, end: 126 } }, 
                            op: BinOp::Rem, expected: Type::Int, full_span: Span { start: 122, end: 130 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Access(TLoc { 
                                var: Var { id: 2, span: Span { start: 143, end: 144 } }, 
                                kind: TLocKind::Var, span: Span { start: 143, end: 144 }, 
                                typ: Type::Record 
                            }), typ: Type::Record, span: Span { start: 143, end: 144 } }, 
                            op: BinOp::Times, expected: Type::Int, full_span: Span { start: 139, end: 144 } 
                        },
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Access(TLoc { 
                                var: Var { id: 3, span: Span { start: 153, end: 154 } }, 
                                kind: TLocKind::Var, span: Span { start: 153, end: 154 }, 
                                typ: Type::IntArr(10) 
                            }), typ: Type::IntArr(10), span: Span { start: 153, end: 154 } }, 
                            op: BinOp::Minus, expected: Type::Int, full_span: Span { start: 153, end: 158 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(false), typ: Type::Bool, span: Span { start: 173, end: 178 } }, 
                            op: BinOp::Lt, expected: Type::Int, full_span: Span { start: 169, end: 178 } 
                        },
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 189, end: 193 } }, 
                            op: BinOp::GtEq, expected: Type::Int, full_span: Span { start: 189, end: 198 } 
                        },
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 209, end: 213 } }, 
                            op: BinOp::LtEq, expected: Type::Int, full_span: Span { start: 209, end: 222 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(false), typ: Type::Bool, span: Span { start: 217, end: 222 } }, 
                            op: BinOp::LtEq, expected: Type::Int, full_span: Span { start: 209, end: 222 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 238, end: 242 } }, 
                            op: BinOp::Lt, expected: Type::Int, full_span: Span { start: 233, end: 242 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(false), typ: Type::Bool, span: Span { start: 259, end: 264 } }, 
                            op: BinOp::Eq, expected: Type::Int, full_span: Span { start: 253, end: 264 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Bool(true), typ: Type::Bool, span: Span { start: 281, end: 285 } }, 
                            op: BinOp::NotEq, expected: Type::Int, full_span: Span { start: 275, end: 285 } 
                        },
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Int(13), typ: Type::Int, span: Span { start: 296, end: 298 } }, 
                            op: BinOp::Or, expected: Type::Bool, full_span: Span { start: 296, end: 303 } 
                        },
                        TypeError::InvalidRightArgument { 
                            argument: TExpr { kind: TExprKind::Int(14), typ: Type::Int, span: Span { start: 301, end: 303 } }, 
                            op: BinOp::Or, expected: Type::Bool, full_span: Span { start: 296, end: 303 } 
                        },
                        TypeError::InvalidLeftArgument { 
                            argument: TExpr { kind: TExprKind::Int(15), typ: Type::Int, span: Span { start: 314, end: 316 } }, 
                            op: BinOp::And, expected: Type::Bool, full_span: Span { start: 314, end: 324 } }
                    ]);
                }
            }
        }
    }
}
