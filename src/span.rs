//! Module with types and functions to refer to places inside the source code 
//! of a compiled program.

use std::cmp;

/// An area inside the source text of a compiled program.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Span {
    /// The start byte-offset of the text.
    pub start: usize,
    
    /// The end byte-offset of the text (exclusive).
    pub end: usize,
}

impl Span {
    /// Creates a span that encompasses both this span and the given one.
    pub fn until(&self, other: Span) -> Span {
        let start = cmp::min(self.start, other.start);
        let end = cmp::max(self.end, other.end);
        Span { start, end }
    } 
}

/// Returns the line and column inside the given source text that the given
/// byte position represents.
/// 
/// Used to show the user where errors were found.
pub fn text_pos(pos: usize, text: &str) -> (usize, usize) {
    let mut line = 1;
    let mut line_start = 0;
    for (i, ch) in text.char_indices() {
        if i >= pos {
            break;
        }
        if ch == '\n' {
            line += 1;
            line_start = i + 1;
        }
    }
    assert!(pos >= line_start);
    let col = (pos - line_start) + 1;
    (line, col)
}

