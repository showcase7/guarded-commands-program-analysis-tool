//! A virtual machine to execute MicroC programs.

use instructions::Instr;

use std::io::{self, Write};

/// Status codes for the virtual machine.
pub mod status {
    pub const MISSING_STACK_VALUE: i64 = 1;
    pub const INVALID_ADDRESS: i64 = 2;
    pub const DIVISION_BY_ZERO: i64 = 3;
    pub const REMAINDER_OF_DIV_BY_ZERO: i64 = 4;
    pub const INVALID_JUMP_TARGET: i64 = 5;
    pub const UNCONVERTED_SYMBOLIC_LABEL: i64 = 6;
    pub const UNCONVERTED_SYMBOLIC_JUMP: i64 = 7;
    pub const INVALID_NUMBER_INPUT: i64 = 8;
    pub const STDIN_READ_FAILED: i64 = 9;
}

pub struct Machine<'ins> {
    pub stack: Vec<i64>,
    pub pc: usize,
    pub program: &'ins [Instr],
    pub status_code: i64,
}

impl<'ins> Machine<'ins> {
    pub fn new(program: &'ins [Instr]) -> Machine<'ins> {
        Machine {
            stack: Vec::new(),
            pc: 0,
            program: program,
            status_code: 0,
        }
    }
    
    #[inline]
    fn exit<T>(&mut self, code: i64) -> Result<T, ()> {
        self.status_code = code;
        self.pc = self.program.len();
        Err(())
    }
    
    #[inline]
    fn pop(&mut self) -> Result<i64, ()> {
        if let Some(value) = self.stack.pop() {
            Ok(value)
        } else {
            self.exit(status::MISSING_STACK_VALUE)
        }
    }
    
    #[inline]
    fn peek(&mut self) -> Result<i64, ()> {
        if let Some(&value) = self.stack.last() {
            Ok(value)
        } else {
            self.exit(status::MISSING_STACK_VALUE)
        }
    }
    
    #[inline]
    fn validate_address(&mut self, addr: i64) -> Result<(), ()> {
        if (addr as usize) < self.stack.len() {
            Ok(())
        } else {
            return self.exit(status::INVALID_ADDRESS);
        }
    }
    
    #[inline]
    pub fn interpret(&mut self, instr: Instr) -> Result<(), ()> {
        use instructions::Instr::*;
        
        match instr {
            Push(value) => self.stack.push(value),
    
            // Duplicates the top value on the stack
            Dup => {
                let value = self.peek()?;
                self.stack.push(value);
            },
    
            // Pops the top value and pushes the value found at this stack address.
            Load => {
                let addr = self.pop()?;
                self.validate_address(addr)?;
                let value = self.stack[addr as usize];
                self.stack.push(value);
            },
    
            // Swaps the two top values of the stack.
            Swap => {
                if self.stack.len() < 2 {
                    return self.exit(status::MISSING_STACK_VALUE);
                }
                let top = self.stack.len() - 1;
                let next = self.stack.len() - 2;
                self.stack.swap(top, next);
            },
    
            // Pops the top value and stores it at the address of the new top value.
            Store => {
                let value = self.pop()?;
                let addr = self.peek()?;
                self.validate_address(addr)?;
                self.stack[addr as usize] = value;
            },
    
            // Pops the top value of the stack.
            Pop => {
                self.pop()?;
            },
    
            //  ======================= Arithmetic ======================= 
            // Pops the top two values on the stack and pushes next + top
            Add => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(left.wrapping_add(right));
            },
            Sub => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(left.wrapping_sub(right));
            },
            Mul => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(left.wrapping_mul(right));
            },
            Div => {
                let right = self.pop()?;
                let left = self.pop()?;
                if left == 0 {
                    return self.exit(status::DIVISION_BY_ZERO);
                }
                self.stack.push(left / right);
            },
            Rem => {
                let right = self.pop()?;
                let left = self.pop()?;
                if left == 0 {
                    return self.exit(status::REMAINDER_OF_DIV_BY_ZERO);
                }
                self.stack.push(left % right);
            },
    
            //  ======================= Arithmetic immediate ======================= 
            //AddI(i64),
            //SubI(i64),
            //MulI(i64),
            //DivI(i64),
            //RemI(i64),
    
            //  ======================= Logical ======================= 
    
            // Pops the two top values and pushes 1 if next < top or 0 otherwise
            Lt => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(if left < right { 1 } else {0});
            },
    
            // Pops the two top values and pushes 1 if next > top or 0 otherwise
            Gt => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(if left > right { 1 } else {0});
            },
    
            // Pops the two top values and pushes 1 if next == top or 0 otherwise
            Eq => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(if left == right { 1 } else {0});
            },
    
            // Pops the top value and pushes 1 if it is 0 or 0 otherwise
            Not => {
                let value = self.pop()?;
                self.stack.push(if value == 0 { 1 } else { 0 });
            },
    
            // Pops the two top values and pushes 1 if they are both not 0 or 0 otherwise
            And => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(if left != 0 && right != 0 { 1 } else {0});
            },
    
            // Pops the two top values and pushes 1 if either is not 0 or 0 otherwise
            Or => {
                let right = self.pop()?;
                let left = self.pop()?;
                self.stack.push(if left != 0 || right != 0 { 1 } else {0});
            },
    
            // ======================= Flow control ======================= 
    
            // Jumps unconditionally to the given instruction number
            Jump(index) => {
                if index >= self.program.len() {
                    return self.exit(status::INVALID_JUMP_TARGET);
                }
                self.pc = index;
            },
    
            // Pops the top stack value and jumps to the given instruction number
            // if the value is not 0.
            JumpIfNotZero(index) => {
                if index >= self.program.len() {
                    return self.exit(status::INVALID_JUMP_TARGET);
                }
                let value = self.pop()?;
                if value != 0 {
                    self.pc = index;
                }
            },
    
            // ===================== Symbolic flow control ==================
            Label(_) => {
                return self.exit(status::UNCONVERTED_SYMBOLIC_LABEL);
            },
    
            JumpToLabel(_) => {
                return self.exit(status::UNCONVERTED_SYMBOLIC_JUMP);
            },
    
            JumpToLabelIfNotZero(_) => {
                return self.exit(status::UNCONVERTED_SYMBOLIC_JUMP);
            },
    
    
            //  ================= Syscall / Env / Lang-specific ====================
    
            // Pops the top stack value and prints it to stdout
            Write => {
                let value = self.pop()?;
                println!("{}", value);
            }, 
    
            // Reads an integer value from stdin and places it on the stack
            Read => {
                eprint!("input> ");
                let stdout = io::stdout();
                let _ = stdout.lock().flush();
                let mut input = String::new();
                match io::stdin().read_line(&mut input) {
                    Ok(_) => {
                        match input.trim().parse::<i64>() {
                            Ok(value) => {
                                self.stack.push(value);
                            }
                            Err(_) => {
                                return self.exit(status::INVALID_NUMBER_INPUT);
                            }
                        }
                    }
                    Err(_) => {
                        return self.exit(status::STDIN_READ_FAILED);
                    },
                }
            }, 
    
            // Does nothing.
            Skip => {},
    
            // Aborts the program with the given status code.
            Exit(code) => {
                return self.exit(code);
            },
        }
        
        self.pc += 1;
        
        Ok(())
    }
    
    pub fn run(&mut self) -> Option<i64> {
        while self.pc < self.program.len() {
            let instr = self.program[self.pc];
            let _ = self.interpret(instr);
        }
        if self.status_code != 0 {
            Some(self.status_code)
        } else {
            None
        }
    }
}

/// Executes the given virtual program.
pub fn execute(program: &[Instr]) -> Option<i64> {
    let mut machine = Machine::new(program);
    machine.run()
}

