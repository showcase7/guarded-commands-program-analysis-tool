//! Module to convert a stream of tokens from a Lexer into an untyped
//! Abstract Syntax Tree.

use ast::{self, *};
use span::{Span, text_pos};
use lexer::{Token, TokenKind, Lexer, LexError, LexResult};


enum DecOrStmt {
    Declaration(Dec),
    Statement(Stmt),
}

enum ExprOrBinOp {
    Expression(Expr),
    Operator(BinOp),
}

/// Different types of parse errors.
#[derive(Debug, Clone, Copy)]
pub enum ParseErrorKind {
    Lex,
    Parse,
}

/// An error found when parsing a source file.
#[derive(Debug, Clone)]
pub struct ParseError<'src> {
    /// The text of the source file.
    pub source: &'src str,
    
    /// The byte offset where the error was found.
    pub position: usize,
    
    /// The line where the error was found. (1-indexed).
    pub line: usize,
    
    /// The column where the error was found. (1-indexed).
    pub col: usize,
    
    /// A message describing the error.
    pub message: String,
    
    /// Additional information about the error.
    pub kind: ParseErrorKind,
}

impl<'src> From<LexError<'src>> for ParseError<'src> {
    fn from(value: LexError<'src>) -> ParseError<'src> {
        ParseError {
            source: value.source,
            position: value.position,
            line: value.line,
            col: value.col,
            message: value.message,
            kind: ParseErrorKind::Lex,
        }
    }
}

/* microc
    "*": 5, "/": 5, "%": 5,
    "+": 4, "-": 4,
    "==": 3 ,"!=": 3, "<": 3, ">": 3, "<=": 3, ">=": 3,
    "&&": 2,
    "||": 1,
*/

/// Returns the priority of the given binary operator.
/// The stronger the operator is, the 'earlier' it binds two values.
fn priority(op: BinOp) -> usize {
    use ast::BinOp::*;
    match op {
        Times | Div | Rem => 5,
        Plus | Minus => 4,
        Eq | NotEq | Lt | Gt | LtEq | GtEq => 3,
        And => 2,
        Or => 1,
    }
}

const NO_PRIORITY: usize = 0;

fn reduce_two(expr_stack: &mut Vec<Expr>, op_stack: &mut Vec<BinOp>) -> usize {
    let right = expr_stack.pop().unwrap();
    let op = op_stack.pop().unwrap();
    let left = expr_stack.pop().unwrap();
    let span = left.span.until(right.span);
    let reduced = Expr {
        kind: ExprKind::Binary(Box::new(left), op, Box::new(right)),
        span: span,
    };
    expr_stack.push(reduced);
    if let Some(&op) = op_stack.last() {
        priority(op)
    } else {
        NO_PRIORITY
    }
}

#[inline]
fn reduce_all(expr_stack: &mut Vec<Expr>, op_stack: &mut Vec<BinOp>) -> Expr {
    while expr_stack.len() > 1 {
        reduce_two(expr_stack, op_stack);
    }
    expr_stack.pop().unwrap()
}

fn order_binary_expression(seq: Vec<ExprOrBinOp>) -> Expr {
    let mut last_prio = NO_PRIORITY;
    let mut expr_stack = Vec::with_capacity(2);
    let mut op_stack = Vec::with_capacity(4);
    for item in seq {
        match item {
            ExprOrBinOp::Expression(expr) => {
                expr_stack.push(expr);
            }
            ExprOrBinOp::Operator(binop) => {
                let prio = priority(binop);
                while last_prio >= prio {
                    last_prio = reduce_two(&mut expr_stack, &mut op_stack);
                }
                op_stack.push(binop);
                last_prio = prio;
            }
        }
    }

    reduce_all(&mut expr_stack, &mut op_stack)
}

pub struct Parser<'src> {
    lexer: Lexer<'src>,
    peek: Option<LexResult<'src, Token>>,
    errors: Vec<ParseError<'src>>,
    source: &'src str,
    // Whether the current parse must be discarded (too much ambiguity)
    parse_invalidated: bool,
}

impl<'src> Parser<'src> {
    /// Creates a new parser to parse the given source file.
    pub fn new(source: &'src str) -> Parser<'src> {
        Parser {
            source: source,
            lexer: Lexer::new(source),
            peek: None,
            errors: Vec::new(),
            parse_invalidated: false,
        }
    }
    
    // ======================= Utility functions ===============================
    
    fn error<T: Into<String>>(&self, kind: ParseErrorKind, pos: usize, message: T) -> ParseError<'src> {
        let (line, col) = text_pos(pos, self.source);
        ParseError {
            source: self.source,
            position: pos,
            line: line,
            col: col,
            message: message.into(),
            kind: kind,
        }
    }
    
    fn simple_error<T: Into<String>>(&mut self, message: T) -> ParseError<'src> {
        let pos = self.lexer.cur_pos();
        self.error(ParseErrorKind::Parse, pos, message)
    }
    
    /// Adds a minor error that doesn't influence the parsing.
    fn add_soft_error(&mut self, error: ParseError<'src>) {
        self.errors.push(error);
    }
    
    /// Adds an error which means that the current item being parsed is too 
    /// ambigous and that the parser should synchronize.
    fn add_hard_error<T>(&mut self, error: ParseError<'src>) -> Result<T, ()> {
        self.errors.push(error);
        Err(())
    }
    
    fn advance(&mut self) -> Result<Token, ()> {
        let res = self.peek.take().unwrap_or_else(|| {
            self.lexer.next().expect("Advance called after EOS token was reached")
        });
        match res {
            Ok(token) => {
                Ok(token)
            }
            Err(lex_err) => {
                self.add_hard_error(lex_err.into())
            }
        }
    }
    
    fn expect<S: Into<String>>(&mut self, kind: TokenKind, expected: S) -> Result<Token, ()> {
        let token = self.advance()?;
        if token.kind != kind {
            let error = self.simple_error(format!("Expected {}", expected.into()));
            self.add_hard_error(error)
        } else {
            Ok(token)
        }
    }
    
    // enter_scope
    // exit scope
    
    fn peek(&mut self) -> Result<Token, ()> {
        let has_peek = self.peek.is_some();
        if ! has_peek {
            self.peek = self.lexer.next();
        }
        match self.peek {
            Some(Ok(token)) => {
                Ok(token)
            }
            Some(Err(_)) => {
                Err(())
            }
            None => {
                panic!("Peek called after EOS token was reached");
            }
        }
    }
    
    fn at_eos(&mut self) -> bool {
        match self.peek() {
            Ok(token) => {
                token.kind == TokenKind::Eos
            }
            _ => false
        }
    }
    
    fn invalidate(&mut self) {
        self.parse_invalidated = true;
    }
    
    /// Puts the given token back at the top of the stream as the next token.
    fn put_back(&mut self, token: Token) {
        if self.peek.is_some() {
            panic!("put_back called with peek already occupied");
        }
        self.peek = Some(Ok(token));
    }
    
    #[inline]
    fn text(&self, token: Token) -> String {
        (&self.source[token.span.start .. token.span.end]).to_string()
    }
    
    fn int(&self, token: Token) -> i64 {
        assert!(token.kind == TokenKind::Integer, "Parser.int() called with non-integer token");
        
        let text = &self.source[token.span.start .. token.span.end];
        text.parse::<i64>().expect("Invalid i64 was parsed as correct")
    }
    
    fn ident(&self, token: Token) -> Ident {
        assert!(token.kind == TokenKind::Ident);
        
        let text = self.text(token);
        let span = token.span;
        Ident { text, span }
    }
    
    /// Moves the token stream to the next unambigous starting point (?)
    //fn synchronize(&mut self) {}
    
    // fn expect(&mut self, kind: TokenKind) -> Result<Token, ()> {}
    
    // ===================== Parsing functions ==================================
    
    fn parse_declaration(&mut self, token: Token) -> Result<Dec, ()> {
        use lexer::TokenKind::*;
        use lexer::Kw::*;
        
        match token.kind {
            Keyword(Int) => {
                let next = self.advance()?;
                match next.kind {
                    Ident => {
                        self.expect(Semicolon, "';' to end declaration")?;
                        Ok(Dec {
                            name: self.ident(next),
                            kind: DecKind::Int,
                            span: token.span.until(next.span)
                        })
                    }
                    BrackOpen => {
                        let size = self.expect(Integer, "array size")?;
                        self.expect(BrackClose, "']'")?;
                        let ident = self.expect(Ident, "variable name")?;
                        self.expect(Semicolon, "';' to end declaration")?;
                        Ok(Dec {
                            name: self.ident(ident),
                            kind: DecKind::IntArr(self.int(size) as usize),
                            span: token.span.until(ident.span)
                        })
                    }
                    _ => {
                        let error = self.simple_error("Expected '[' or variable name after 'int' keyword");
                        self.add_hard_error(error)
                    }
                }
            }
            CurlyOpen => {
                self.expect(Keyword(Int), "'int'")?;
                self.expect(Keyword(Fst), "'fst'")?;
                self.expect(Semicolon, "';'")?;
                self.expect(Keyword(Int), "'int'")?;
                self.expect(Keyword(Snd), "'snd'")?;
                self.expect(CurlyClose, "'}'")?;
                let ident = self.expect(Ident, "variable name")?;
                self.expect(Semicolon, "';' to end declaration")?;
                Ok(Dec {
                    name: self.ident(ident),
                    kind: DecKind::Record,
                    span: token.span.until(ident.span),
                })
            }
            _ => unreachable!(),
        }
    }
    
    fn parse_single(&mut self) -> Result<Expr, ()> {
        use lexer::TokenKind::*;
        use lexer::Kw::*;
        
        let token = self.advance()?;
        Ok(match token.kind {
            Integer => {
                Expr {
                    kind: ExprKind::Int(self.int(token)),
                    span: token.span,
                }
            }
            Ident => {
                let loc = self.parse_location(token)?;
                let span = loc.span;
                Expr {
                    kind: ExprKind::Access(loc),
                    span: span,
                }
            }
            Keyword(True) => {
                Expr {
                    kind: ExprKind::Bool(true),
                    span: token.span,
                }
            }
            Keyword(False) => {
                Expr {
                    kind: ExprKind::Bool(false),
                    span: token.span,
                }
            }
            Keyword(Not) => {
                let expr = self.parse_single()?;
                let span = token.span.until(expr.span);
                Expr {
                    kind: ExprKind::Unary(UnOp::Not, Box::new(expr)),
                    span: span,
                }
            }
            ParOpen => {
                let first = self.parse_expression()?;
                self.expect(Comma, "',' between record values")?;
                let second = self.parse_expression()?;
                let closing = self.expect(ParClose, "')'")?;
                let span = token.span.until(closing.span);
                Expr {
                    kind: ExprKind::Record(Box::new(first), Box::new(second)),
                    span: span,
                }
            }
            _ => {
                self.put_back(token);
                let error = self.simple_error("Expected expression");
                return self.add_hard_error(error);
            }
        })
    }
    
    fn parse_expression(&mut self) -> Result<Expr, ()> {
        use lexer::TokenKind::*;
        
        fn binop_from_token(token: Token) -> Option<BinOp> {
            match token.kind {
                Plus        => Some(BinOp::Plus),
                Minus       => Some(BinOp::Minus),
                Star        => Some(BinOp::Times),
                Slash       => Some(BinOp::Div),
                Percent     => Some(BinOp::Rem),
                
                Lt          => Some(BinOp::Lt),
                Gt          => Some(BinOp::Gt),
                LtEq        => Some(BinOp::LtEq),
                GtEq        => Some(BinOp::GtEq),
                Eq          => Some(BinOp::Eq),
                NotEq       => Some(BinOp::NotEq),
                
                Ampersand   => Some(BinOp::And),
                VLine       => Some(BinOp::Or),
                _           => None,
            }
        }
        
        let mut op_list = Vec::new();
        loop {
            let expr = self.parse_single()?;
            
            op_list.push(ExprOrBinOp::Expression(expr));
            
            let peek = self.peek()?;
            if let Some(op) = binop_from_token(peek) {
                let _ = self.advance();
                op_list.push(ExprOrBinOp::Operator(op));
            } else {
                break;
            }
        }
        
        if op_list.len() == 1 {
            if let Some(ExprOrBinOp::Expression(expr)) = op_list.pop() {
                Ok(expr)
            } else {
                unreachable!();
            }
        } else {
            Ok(order_binary_expression(op_list))
        }
    }
    
    fn parse_location(&mut self, ident: Token) -> Result<Loc, ()> {
        use lexer::TokenKind::*;
        use lexer::Kw::*;
        
        let next = self.peek()?;
        match next.kind {
            BrackOpen => {
                let _ = self.advance();
                let expr = self.parse_expression()?;
                let close = self.expect(BrackClose, "']'")?;
                Ok(Loc {
                    name: self.ident(ident),
                    kind: LocKind::Arr(Box::new(expr)),
                    span: next.span.until(close.span),
                })
            }
            Dot => {
                let _ = self.advance();
                let token = self.advance()?;
                match token.kind {
                    Keyword(Fst) | Keyword(Snd) => {
                        let field_name = self.text(token);
                        let field_span = token.span;
                        let field = ast::Ident {
                            text: field_name,
                            span: field_span
                        };
                        
                        let span = ident.span.until(token.span);
                        Ok(Loc {
                            name: self.ident(ident),
                            kind: LocKind::Field(field),
                            span: span,
                        })
                    }
                    _ => {
                        self.put_back(token);
                        let error = self.simple_error("Expected 'fst' or 'snd' as field name");
                        self.add_hard_error(error)
                    }
                }
            }
            
            _ => Ok(Loc {
                name: self.ident(ident),
                kind: LocKind::Var,
                span: ident.span,
            })
        }
    }
    
    fn parse_block(&mut self) -> Result<(Vec<Stmt>, Span), ()> {
        let open = self.expect(TokenKind::CurlyOpen, "'{' to start block")?;
        let mut statements = Vec::new();
        let span;
        loop {
            let token = self.advance()?;
            if let TokenKind::CurlyClose = token.kind {
                span = open.span.until(token.span);
                break;
            }
            let stmt = self.parse_statement(token)?;
            statements.push(stmt);
        }
        Ok((statements, span))
    }
    
    fn parse_statement(&mut self, token: Token) -> Result<Stmt, ()> {
        use lexer::TokenKind::*;
        use lexer::Kw::*;
        
        //unimplemented!();
        Ok(match token.kind {
            Ident => {
                let loc = self.parse_location(token)?;
                self.expect(ColonEq, "':=' after location")?;
                let expr = self.parse_expression()?;
                self.expect(Semicolon, "';' to end statement")?;
                let span = loc.span.until(expr.span);
                Stmt {
                    kind: StmtKind::Assign(loc, expr),
                    span: span,
                }
            }
            Keyword(If) => {
                self.expect(ParOpen, "'('")?;
                let expr = self.parse_expression()?;
                self.expect(ParClose, "')'")?;
                let (body, body_span) = self.parse_block()?;
                let peek = self.peek()?;
                let (elsebody, span) = if let Keyword(Else) = peek.kind {
                    self.advance()?;
                    let (elsebody, elsebody_span) = self.parse_block()?;
                    (Some(elsebody), token.span.until(elsebody_span))
                } else {
                    (None, token.span.until(body_span))
                };
                Stmt {
                    kind: StmtKind::If(expr, body, elsebody),
                    span: span,
                }
            }
            Keyword(While) => {
                self.expect(ParOpen, "'('")?;
                let expr = self.parse_expression()?;
                self.expect(ParClose, "')'")?;
                let (body, body_span) = self.parse_block()?;
                Stmt {
                    kind: StmtKind::While(expr, body),
                    span: token.span.until(body_span),
                }
            }
            Keyword(Break) => {
                self.expect(Semicolon, "';' after statement")?;
                Stmt {
                    kind: StmtKind::Break,
                    span: token.span,
                }
            }
            Keyword(Read) => {
                let next = self.advance()?;
                if let Ident = next.kind {
                    let loc = self.parse_location(next)?;
                    self.expect(Semicolon, "';' after statement")?;
                    let span = token.span.until(loc.span);
                    Stmt {
                        kind: StmtKind::Read(loc),
                        span: span,
                    }
                } else {
                    self.put_back(token);
                    let error = self.simple_error("Expected location to read into");
                    return self.add_hard_error(error);
                }
            }
            Keyword(Write) => {
                let expr = self.parse_expression()?;
                self.expect(Semicolon, "';' after statement")?;
                let span = token.span.until(expr.span);
                Stmt {
                    kind: StmtKind::Write(expr),
                    span: span,
                }
            }
            _ => {
                self.put_back(token);
                let error = self.simple_error("Expected statement");
                return self.add_hard_error(error);
            }
        })
    }
    
    fn parse_top_level(&mut self) -> Result<DecOrStmt, ()> {
        use lexer::TokenKind::*;
        use lexer::Kw::*;
        
        match self.advance() {
            Ok(token) => {
                match token.kind {
                    Eos => unreachable!(),
                    
                    // Declaration
                    Keyword(Int) | CurlyOpen => {
                        self.parse_declaration(token).map(DecOrStmt::Declaration)
                    }
                    
                    // Statement
                    Ident | 
                    Keyword(If) | Keyword(While) | Keyword(Break) |
                    Keyword(Read) | Keyword(Write) => {
                        self.parse_statement(token).map(DecOrStmt::Statement)
                    }
                    
                    // Other
                    _ => {
                        self.put_back(token);
                        let error = self.error(ParseErrorKind::Parse, token.span.start, "Expected declaration or statement");
                        self.add_hard_error(error)
                    },
                }
            }
            Err(()) => {
                Err(())
            }
        }
    }
    
    fn recover_at_top_level(&mut self) {
        
        while ! self.at_eos() {
            if let Ok(token) = self.peek() {
                if let TokenKind::CurlyClose = token.kind {
                    break;
                }
            }
            let _ = self.advance();
        }
        
        return;
        
        /*let mut scope_stack: Vec<Token> = Vec::new();
        while ! self.at_eos() {
            
            // sync based on parens too?
            match self.peek() {
                Ok(token) => {
                    match (scope_stack.last().map(|&t| t.kind), token.kind) {
                        (None, Semicolon) => {
                            return;
                        }
                        // Declaration init tokens
                        (None, Keyword(Int)) | 
                        (None, CurlyOpen) |
                        // Statement init tokens
                        (None, Keyword(Write)) | 
                        (None, Keyword(Read)) | 
                        (None, Keyword(For)) | 
                        (None, Keyword(While)) | 
                        (None, Keyword(If)) => {
                        
                        }
                        _ => unimplemented!(),
                    }
                }
                Err(_) => {
                    self.advance();
                }
            }
        }*/
    }
    
    /// Parses the source file given to the parser.
    pub fn parse(mut self) -> Result<Program, (Program, Vec<ParseError<'src>>)> {
        
        // parse zero or more declarations
        // parse at least one statement
        
        let mut declarations = Vec::new();
        let mut statements = Vec::new();
        
        let _ = self.expect(TokenKind::CurlyOpen, "Opening '}'");
        
        loop {
            if self.at_eos() {
                break;
            } else {
                if let Ok(token) = self.peek() {
                    if let TokenKind::CurlyClose = token.kind {
                        let _ = self.advance();
                        break;
                    }
                }
                match self.parse_top_level() {
                    Ok(DecOrStmt::Declaration(dec)) => {
                        if ! statements.is_empty() {
                            let error = self.simple_error("/|\\ Declaration after statements");
                            self.add_soft_error(error);
                        }
                        declarations.push(dec);
                    }
                    Ok(DecOrStmt::Statement(stmt)) => {
                        statements.push(stmt);
                    }
                    Err(()) => {
                        // balance the parens and look for a valid end 
                        // (either close curly or semicolon)
                        self.recover_at_top_level();
                    }
                }
            }
        }
        
        if ! self.at_eos() {
            let error = self.simple_error("Found tokens after closing '}' of the program");
            self.add_soft_error(error);
        }
        
        let program = Program {
            declarations, statements,
        };
        if ! self.errors.is_empty() {
            Err((program, self.errors))
        } else {
            Ok(program)
        }
    }
}

/// Parses the given source text into an untyped Abstract Syntax Tree.
pub fn parse<'src>(source: &'src str) -> Result<Program, (Program, Vec<ParseError<'src>>)> {
    let parser = Parser::new(source);
    parser.parse()
}
