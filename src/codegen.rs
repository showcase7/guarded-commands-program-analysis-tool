//! Module to generate virtual instructions from a typed program.

use ast::*;
use instructions::{LabelId, Instr};

#[derive(Debug, Clone, Copy)]
pub struct VarLoc {
    pub scope: Scope,
    pub offset: usize,
    pub typ: Type,
}

#[derive(Debug, Clone, Copy)]
pub enum Scope {
    Local,
    Global,
}

#[derive(Debug, Clone, Copy)]
pub struct Context {
    /// What kind of scope this is in.
    pub scope: Scope,
    
    /// How much space the local variables take up on the stack
    pub var_len: usize,
}

/// Code generator object. Contains the state needed to generate instructions.
#[derive(Debug, Clone)]
pub struct CodeGen {
    pub instructions: Vec<Instr>,
    pub env: Vec<VarLoc>,
    pub next_label_id: LabelId,
    pub context: Context,
    // context_history
}

impl CodeGen {
    pub fn new() -> CodeGen {
        CodeGen {
            instructions: Vec::new(),
            env: Vec::new(),
            next_label_id: 1,
            context: Context {
                scope: Scope::Global,
                var_len: 0,
            }
        }
    }
    
    // ============== Helper functions ===================
    
    /// Adds the given instruction to the list
    fn push(&mut self, instr: Instr) {
        self.instructions.push(instr);
    }
    
    /// Generates a new jump label.
    fn new_label(&mut self) -> LabelId {
        let id = self.next_label_id;
        self.next_label_id += 1;
        id
    }
    
    /// Binds a name to a variable in the current context.
    fn bind(&mut self, var: Var, loc: VarLoc) {
        assert!(var.id == self.env.len());
        self.env.push(loc);
    }
    
    fn lookup(&mut self, var: &Var) -> VarLoc {
        *self.env.get(var.id).expect("Type-check did not catch unbound variables")
    }
    
    /// Binds the given variable in the given scope and adds instructions
    /// to load it.
    fn allocate(&mut self, scope: Scope, var: Var, typ: Type) {
        use ast::Type::*;
        
        match scope {
            Scope::Global => {
                let loc = VarLoc {
                    scope: scope,
                    offset: self.context.var_len,
                    typ: typ,
                };
                self.bind(var, loc);
                
                match typ {
                    Int => {
                        self.push(Instr::Push(0));
                        self.context.var_len += 1;
                    }
                    IntArr(len) => {
                        let addr = self.context.var_len + 1;
                        self.push(Instr::Push(addr as i64));
                        self.push(Instr::Push(len as i64));
                        for _ in 0..len {
                            self.push(Instr::Push(0));
                        }
                        self.context.var_len += 2 + len;
                    }
                    Record => {
                        self.push(Instr::Push(0));
                        self.push(Instr::Push(0));
                        self.context.var_len += 2;
                    }
                    Bool => unreachable!(),
                    RecordExpr => unreachable!(),
                }
            }
            Scope::Local => unimplemented!(),
        }
    }
    
    /// Converts the symbolic label jumps to fixed index jumps.
    pub fn convert_to_fixed_jumps(mut instrs: Vec<Instr>, num_labels: usize) -> Vec<Instr> {
        let mut label_addrs = vec![0 ; num_labels];
        let mut jump_instr_indices = Vec::new();
        
        // Find jump and label symbolic instructions
        for (i, instr) in instrs.iter_mut().enumerate() {
            match *instr {
                Instr::Label(id) => {
                    label_addrs[id] = i;
                    // Do I even need to do this?
                    *instr = Instr::Skip;
                }
                Instr::JumpToLabel(_) | Instr::JumpToLabelIfNotZero(_) => {
                    jump_instr_indices.push(i);
                }
                _ => {}
            }
        }
        
        // replace the jumps with the label addresses
        for i in jump_instr_indices {
            match instrs[i] {
                Instr::JumpToLabel(id) => {
                    instrs[i] = Instr::Jump(label_addrs[id]);
                }
                Instr::JumpToLabelIfNotZero(id) => {
                    instrs[i] = Instr::JumpIfNotZero(label_addrs[id]);
                }
                _ => unreachable!(),
            }
        }
        
        instrs
    }
    
    // ============== Generator functions ===================
    
    /// Generates instructions to create the address of the given location
    /// on top of the stack.
    pub fn gen_loc(&mut self, loc: &TLoc) {
        use ast::TLocKind::*;
        use instructions::Instr::*;
        
        let varloc = self.lookup(&loc.var);
        
        match loc.kind {
            Var => {
                match varloc.scope {
                    Scope::Global => {
                        self.push(Instr::Push(varloc.offset as i64));
                    }
                    _ => unimplemented!(),
                }
            }
    
            Arr(ref index) => {
                /*
                gen index
                dup
                if index < 0:
                  goto err
                gen addr
                load
                if next < top:
                  goto okay
            err:
                exit 102
           okay:
                gen addr
                add
                1
                add
                */
                
                let okay_label = self.new_label();
                let err_label = self.new_label();
                
                self.gen_expr(index); // [.., index]
                
                // Ensure that index >= 0
                self.push(Dup); // [.., index, index]
                self.push(Push(0));
                self.push(Lt);
                self.push(JumpToLabelIfNotZero(err_label));
                
                // Ensure that index < len
                self.push(Dup);
                if let Scope::Global = varloc.scope { // [.., index, index, len_addr]
                    self.push(Push(varloc.offset as i64));
                    self.push(Load);
                }
                self.push(Load); // [.., index, index, len]
                self.push(Lt); // [.., index, index < len]
                self.push(JumpToLabelIfNotZero(okay_label)); // [.., index]
                
                self.push(Label(err_label));
                self.push(Exit(102));
                
                self.push(Label(okay_label));
                
                // Read the actual value
                if let Scope::Global = varloc.scope { // [.., index, len_addr]
                    self.push(Push(varloc.offset as i64));
                    self.push(Load);
                }
                self.push(Add); // [.., index + len_addr]
                self.push(Push(1));
                self.push(Add); // [.., index + len_addr + 1]
            }
    
            // field (fst|snd)
            Field(offset) => {
                match varloc.scope {
                    Scope::Global => {
                        self.push(Instr::Push(varloc.offset as i64));
                    }
                    _ => unimplemented!(),
                }
                match offset {
                    0 => {}
                    other => {
                        self.push(Push(other as i64));
                        self.push(Add);
                    }
                }
            }
        }
    }
    
    pub fn gen_expr(&mut self, expr: &TExpr) {
        use ast::TExprKind::*;
        use instructions::Instr::*;
        
        match expr.kind {
            Int(value) => {
                self.push(Push(value));
            },
            Access(ref loc) => {
                self.gen_loc(loc);
                self.push(Load);
            },
            Bool(true) => {
                self.push(Push(1));
            },
            Bool(false) => {
                self.push(Push(0));
            }
            Binary(ref left, op, ref right) => {
                self.gen_expr(left);
                self.gen_expr(right);
                match op {
                    BinOp::Plus => self.push(Add),
                    BinOp::Minus => self.push(Sub),
                    BinOp::Times => self.push(Mul),
                    BinOp::Div => self.push(Div),
                    BinOp::Rem => self.push(Rem),
                    
                    BinOp::Lt => self.push(Lt),
                    BinOp::LtEq => {
                        self.push(Gt);
                        self.push(Not);
                    },
                    BinOp::Gt => self.push(Gt),
                    BinOp::GtEq => {
                        self.push(Lt);
                        self.push(Not);
                    },
                    BinOp::Eq => self.push(Eq),
                    BinOp::NotEq => {
                        self.push(Eq);
                        self.push(Not);
                    },
                    
                    BinOp::And => self.push(And),
                    BinOp::Or => self.push(Or),
                }
            },
            Unary(op, ref expr) => {
                self.gen_expr(expr);
                match op {
                    UnOp::Not => {
                        self.push(Not);
                    }
                }
            },
            Record(ref fst, ref snd) => {
                self.gen_expr(fst);
                self.gen_expr(snd);
            },
        }
    }
    
    /// Generates instructions to run the given statement.
    pub fn gen_stmt(&mut self, stmt: &TStmt, break_label: LabelId) {
        use ast::TStmtKind::*;
        use instructions::Instr::*;
        
        match stmt.kind {
            Assign(ref loc, ref expr) => {
                self.gen_loc(loc);
                self.gen_expr(expr);
                self.push(Store);
                self.push(Pop);
            },
            If(ref cond, ref body, ref else_body) => {
                /*
                <cond>
                if not cond:
                    goto else
                <if-body>
                goto end
           else:
                <else-body>
           end:          
                */
                let else_label = self.new_label();
                self.gen_expr(cond);
                self.push(Not);
                self.push(JumpToLabelIfNotZero(else_label));
                for stmt in body {
                    self.gen_stmt(stmt, break_label);
                }
                
                if let Some(else_body) = else_body.as_ref() {
                    let end_label = self.new_label();
                    self.push(JumpToLabel(end_label));
                    self.push(Label(else_label));
                    for stmt in else_body {
                        self.gen_stmt(stmt, break_label);
                    }
                    self.push(Label(end_label));
                } else {
                    self.push(Label(else_label));
                }
            },
            While(ref cond, ref body) => {
                /*
                <cond>
                if not cond
                  goto end
          start:
                <while-body>
                <cond>
                if cond:
                  goto start
            end:
                */
                let start_label = self.new_label();
                let end_label = self.new_label();
                self.gen_expr(cond);
                self.push(Not);
                self.push(JumpToLabelIfNotZero(end_label));
                
                self.push(Label(start_label));
                for stmt in body {
                    self.gen_stmt(stmt, end_label);
                }
                self.gen_expr(cond);
                self.push(JumpToLabelIfNotZero(start_label));
                
                self.push(Label(end_label));
            },
            Break => {
                self.push(JumpToLabel(break_label));
            },
            TStmtKind::Read(ref loc) => {
                self.gen_loc(loc);
                self.push(Instr::Read);
                self.push(Store);
                self.push(Pop);
            },
            TStmtKind::Write(ref expr) => {
                self.gen_expr(expr);
                self.push(Instr::Write);
            },
        }
    }
    
    /// Generates the instructions to run the given program.
    pub fn gen_program(&mut self, program: &TProgram) {
        for dec in &program.declarations {
            self.allocate(Scope::Global, dec.var, dec.typ());
        }
        
        for stmt in &program.statements {
            self.gen_stmt(stmt, 0);
        }
    }
}

/// Generates virtual instruction to execute the given program on the virtual
/// machine.
pub fn generate_instructions(program: &TProgram) -> Vec<Instr> {
    let mut codegen = CodeGen::new();
    codegen.gen_program(program);
    CodeGen::convert_to_fixed_jumps(codegen.instructions, codegen.next_label_id)
}

