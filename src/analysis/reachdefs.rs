//! Module for the __Reaching Definitions__ program analysis.

use analysis::{Analysis, BasicAction, BasicLoc, Prograph, ProgEdge, EdgeId, VertexId};
use ast::*;

use std::collections::{HashSet, HashMap};


fn gen_with<F: FnMut(BasicLoc)>(action: &BasicAction, mut gen: F) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref dec) => {
            match dec.kind {
                TDecKind::Int => {
                    gen(BasicLoc::Int(dec.var.id));
                }
                TDecKind::IntArr(_) => {
                    gen(BasicLoc::IntArr(dec.var.id));
                }
                TDecKind::Record => {
                    gen(BasicLoc::Field(dec.var.id, 0));
                    gen(BasicLoc::Field(dec.var.id, 1));
                },
            }
        },
        Test(ref _expr) => {},
        Read(ref loc) | Assignment(ref loc, _) => {
            match loc.kind {
                TLocKind::Var => {
                    match loc.typ {
                        Type::Int => {
                            gen(BasicLoc::Int(loc.var.id));
                        }
                        Type::Record => {
                            gen(BasicLoc::Field(loc.var.id, 0));
                            gen(BasicLoc::Field(loc.var.id, 1));
                        }
                        _ => {
                            unreachable!();
                        }
                    }
                }
                TLocKind::Arr(_) => {
                    gen(BasicLoc::IntArr(loc.var.id));
                }
                TLocKind::Field(offset) => {
                    gen(BasicLoc::Field(loc.var.id, offset));
                }
            }
        }
        Write(ref _expr) => {},
        Skip(ref _name) => {},
    }
}

#[derive(Debug, Clone, Copy)]
enum KillAction {
    Variable(VarId, Type),
    Field(VarId, usize),
    Nothing,
}

impl KillAction {
    /// Returns whether the action kills the given location.
    pub fn kills(&self, loc: BasicLoc) -> bool {
        use analysis::BasicLoc::*;

        match (*self, loc) {
            (KillAction::Nothing, _) => {
                false
            }
            (KillAction::Variable(var, Type::Int),          Int(lvar)) |
            (KillAction::Variable(var, Type::IntArr(_)),    IntArr(lvar)) |
            (KillAction::Variable(var, Type::Record),       Field(lvar, _)) => {
                var == lvar
            }
            (KillAction::Field(var, offset), Field(lvar, loffset)) => {
                var == lvar && offset == loffset
            }
            _ => false,
        }
    }
}

/// Returns a description of what to kill for the given action.
fn kill_action(action: &BasicAction) -> KillAction {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref dec) => {
            KillAction::Variable(dec.var.id, dec.typ())
        }
        Assignment(ref loc, _) | Read(ref loc) => {
            match loc.kind {
                TLocKind::Var => {
                    KillAction::Variable(loc.var.id, loc.typ)
                }
                TLocKind::Arr(_) => {
                    KillAction::Nothing
                }
                TLocKind::Field(offset) => {
                    KillAction::Field(loc.var.id, offset)
                }
            }
        }
        _ => KillAction::Nothing,
    }
}

/// The reaching definitions at a single point in the program.
pub type RdState = HashMap<BasicLoc, HashSet<EdgeId>>;

/// The reaching definitions analysis.
pub struct ReachingDefinitions;

impl ReachingDefinitions {
    pub fn new() -> ReachingDefinitions {
        ReachingDefinitions {}
    }
}

impl Analysis for ReachingDefinitions {
    type State = RdState;
    
    #[inline]
    fn empty_state() -> Self::State {
        RdState::new()
    }
    
    fn initial_state(_start_id: VertexId, program: &Prograph) -> Self::State {
        let mut state = RdState::new();
        for loc in program.basic_locations() {
            let mut set = HashSet::new();
            set.insert(0);
            state.insert(loc.clone(), set);
        }
        state
    }
    
    #[inline(always)]
    fn is_forward_analysis() -> bool {
        true
    }
    
    fn transfer<'a>(&mut self, src: &mut Self::State, edge: &ProgEdge<'a>, dst: &mut Self::State, is_first_visit: bool, _program: &Prograph) -> bool {
        let mut updated = is_first_visit;
        
        // Transfer the ones that aren't killed
        let kill_set = kill_action(&edge.label);
        for (&loc, assignments) in src.iter() {
            if ! kill_set.kills(loc) {
                let mut set = dst.entry(loc.clone()).or_insert_with(HashSet::new);
                let initial_size = set.len();
                set.extend(assignments);
                if set.len() != initial_size {
                    updated = true;
                }
            }
        }
        
        // Generate new definitions
        gen_with(&edge.label, |loc| {
            let set = dst.entry(loc).or_insert_with(HashSet::new);
            let initial_size = set.len();
            set.insert(edge.id);
            if set.len() != initial_size {
                updated = true;
            }
        });
        
        updated
    }
    
}

