//! Module for the __Live Variables__ program analysis.
//!
//! Finds out for each point in the program: Which basic locations are still
//! needed to compute something later in the program, that is, which basic
//! locations are 'alive' at any point in the program.

use analysis::{BasicAction, BasicLoc, Prograph, VertexId, Analysis, ProgEdge};
use ast::*;

use std::collections::{HashSet};

// The free variables in a location that is assigned to.
fn free_vars_loc_with<F: FnMut(BasicLoc)>(loc: &TLoc, f: &mut F) {
    use ast::TLocKind::*;
    
    match loc.kind {
        Var => {},
        Arr(ref expr) => {
            free_vars_expr_with(expr, f);
        },
        Field(_) => {},
    }
}

// The free variables in a location that is accessed for its value.
fn free_vars_acc_with<F: FnMut(BasicLoc)>(loc: &TLoc, f: &mut F) {
    match loc.kind {
        TLocKind::Var => {
            f(BasicLoc::Int(loc.var.id));
        },
        TLocKind::Arr(ref expr) => {
            f(BasicLoc::IntArr(loc.var.id));
            free_vars_expr_with(expr, f);
        },
        TLocKind::Field(offset) => {
            f(BasicLoc::Field(loc.var.id, offset));
        },
    }
}

/// The free variables in the given expression.
fn free_vars_expr_with<F: FnMut(BasicLoc)>(expr: &TExpr, f: &mut F) {
    use ast::TExprKind::*;
    
    match expr.kind {
        Int(_) => {},
        Access(ref loc) => {
            free_vars_acc_with(loc, f);
        },
        Bool(_) => {},
        Binary(ref left, _op, ref right) => {
            free_vars_expr_with(left, f);
            free_vars_expr_with(right, f);
        },
        Unary(_op, ref expr) => {
            free_vars_expr_with(expr, f);
        },
        Record(ref fst, ref snd) => {
            free_vars_expr_with(fst, f);
            free_vars_expr_with(snd, f);
        },
    }
}

/// Generates new live variables.
///
/// ```
/// gen([dec]) = Ø
/// gen([loc := expr]) = FV(loc) U FV(expr)
/// gen([expr]) = FV(expr)
/// gen([skip]) = Ø
/// gen([test]) = FV(test)
/// gen([read loc]) = FV(loc)
/// gen([write expr]) = FV(expr)
/// ```
fn gen_with<F: FnMut(BasicLoc)>(action: &BasicAction, mut gen: F) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref _dec) => {},
        Test(ref expr) => {
            free_vars_expr_with(expr, &mut gen);
        },
        Assignment(ref loc, ref expr) => {
            free_vars_loc_with(loc, &mut gen);
            free_vars_expr_with(expr, &mut gen);
        }
        Read(ref loc) => {
            free_vars_loc_with(loc, &mut gen);
        }
        Write(ref expr) => {
            free_vars_expr_with(expr, &mut gen);
        },
        Skip(ref _name) => {},
    }
}

/// Returns a description of which live variables to kill for the given action.
///
/// ```
/// kill([dec]) = FV(dec)
/// kill([loc := expr]) = FV(loc)
/// kill([expr]) = Ø
/// kill([skip]) = Ø
/// kill([test]) = Ø
/// kill([read loc]) = FV(loc)
/// kill([write expr]) = Ø
/// ```
fn kill_with<F: FnMut(BasicLoc)>(action: &BasicAction, mut kill: F) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref dec) => {
            match dec.kind {
                TDecKind::Int => {
                    kill(BasicLoc::Int(dec.var.id));
                },
                TDecKind::IntArr(_) => {
                    kill(BasicLoc::IntArr(dec.var.id));
                },
                TDecKind::Record => {
                    kill(BasicLoc::Field(dec.var.id, 0));
                    kill(BasicLoc::Field(dec.var.id, 1));
                },
            }
        }
        Assignment(ref loc, _) | Read(ref loc) => {
            match loc.kind {
                TLocKind::Var => {
                    kill(BasicLoc::Int(loc.var.id));
                }
                TLocKind::Arr(ref expr) => {
                    free_vars_expr_with(expr, &mut kill);
                },
                TLocKind::Field(offset) => {
                    kill(BasicLoc::Field(loc.var.id, offset));
                },
            }
        }
        _ => {},
    }
}

/// The live variables at a single point in the program.
pub type LvState = HashSet<BasicLoc>;

pub struct LiveVariables {
    kill_store: Vec<BasicLoc>,
}

impl LiveVariables {
    pub fn new(kill_store_capacity: usize) -> LiveVariables {
        LiveVariables {
            kill_store: Vec::with_capacity(kill_store_capacity),
        }
    }
}

impl Analysis for LiveVariables {
    type State = LvState;
    
    fn empty_state() -> Self::State {
        // We start out assuming that all the variables are dead everywhere
        HashSet::new()
    }
    
    fn initial_state(_start_id: VertexId, _program: &Prograph) -> Self::State {
        // Variables are dead until proven used
        HashSet::new()
    }
    
    fn is_forward_analysis() -> bool {
        // This is a reverse-flow
        false
    }
    
    fn transfer<'a>(&mut self, src: &mut Self::State, edge: &ProgEdge<'a>, dst: &mut Self::State, is_first_visit: bool, program: &Prograph) -> bool {
        // Always update the dependents on the first visit.
        let mut updated = is_first_visit;
        
        // Pop and store the variables that are killed
        kill_with(edge.label, |loc| {
            println!("Killing {}", program.name(loc));
            if src.remove(&loc) {
                self.kill_store.push(loc);
            }
        });
        
        // Transfer variables from the previous state
        let pre_size = dst.len();
        for &loc in src.iter() {
            println!("Transferring {}", program.name(loc));
            dst.insert(loc);
        }
        
        // Restore the killed locations
        for &loc in self.kill_store.iter() {
            src.insert(loc);
        }
        self.kill_store.clear();
        
        // Generate new definitions
        gen_with(edge.label, |loc| {
            println!("Generating {}", program.name(loc));
            dst.insert(loc);
        });
        
        if dst.len() != pre_size {
            updated = true;
        }
        
        updated
    }
    
}

