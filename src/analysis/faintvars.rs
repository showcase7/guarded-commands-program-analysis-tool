//! Module for the __Faint Variables__ program analysis.
//!
//! Finds out for each point in the program: Which basic locations are never
//! needed to compute something later in the program, that is, which basic
//! locations are 'dead' at any point in the program.

use analysis::{Analysis, BasicAction, BasicLoc, Prograph, ProgEdge, VertexId};
use ast::*;

use std::collections::{HashSet};
use std::mem;

// The free variables in a location that is accessed for its value.
fn free_vars_acc_with<F: FnMut(BasicLoc)>(loc: &TLoc, f: &mut F) {
    match loc.kind {
        TLocKind::Var => {
            f(BasicLoc::Int(loc.var.id));
        },
        TLocKind::Arr(ref expr) => {
            f(BasicLoc::IntArr(loc.var.id));
            free_vars_expr_with(expr, f);
        },
        TLocKind::Field(offset) => {
            f(BasicLoc::Field(loc.var.id, offset));
        },
    }
}

/// The free variables in the given expression.
fn free_vars_expr_with<F: FnMut(BasicLoc)>(expr: &TExpr, f: &mut F) {
    use ast::TExprKind::*;
    
    match expr.kind {
        Int(_) => {},
        Access(ref loc) => {
            free_vars_acc_with(loc, f);
        },
        Bool(_) => {},
        Binary(ref left, _op, ref right) => {
            free_vars_expr_with(left, f);
            free_vars_expr_with(right, f);
        },
        Unary(_op, ref expr) => {
            free_vars_expr_with(expr, f);
        },
        Record(ref fst, ref snd) => {
            free_vars_expr_with(fst, f);
            free_vars_expr_with(snd, f);
        },
    }
}

/// Generates new faint variables.
///
/// Variables become faint when they are redefined, since the previous
/// definition now cannot go further down the program.
/// 
/// ```
/// gen(p->n, [int[n] A])               = { A }
/// gen(p->n, [int x])                  = { x }
/// gen(p->n, [{int fst; int snd} R])   = { R.fst, R.snd } // later def?
/// 
/// gen(p->n, [x := expr])              = { x }
/// gen(p->n, [read x])                 = { x }
/// gen(p->n, [R.field := expr])        = { R.field }
/// gen(p->n, [read R.field])           = { R.field }
/// gen(p->n, [R := (a1, a2)])          = { R.fst, R.snd }
/// gen(p->n, [A[index] := expr])       = Ø
/// gen(p->n, [read A[index]])          = Ø
///
/// gen(p->n, [write _])                = Ø
/// gen(p->n, [b])                      = Ø
/// gen(p->n, [skip])                   = Ø
/// ```
fn gen_with<F: FnMut(BasicLoc)>(action: &BasicAction, mut gen: F) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref dec) => {
            match dec.kind {
                TDecKind::Int => {
                    gen(BasicLoc::Int(dec.var.id));
                }
                TDecKind::IntArr(_) => {}
                TDecKind::Record => {
                    gen(BasicLoc::Field(dec.var.id, 0));
                    gen(BasicLoc::Field(dec.var.id, 1));
                }
            }
        },
        Assignment(ref loc, _) | Read(ref loc) => {
            match (&loc.kind, loc.typ) {
                (&TLocKind::Var, Type::Record) => {
                    gen(BasicLoc::Field(loc.var.id, 0));
                    gen(BasicLoc::Field(loc.var.id, 1));
                }
                (&TLocKind::Var, Type::Int) => {
                    gen(BasicLoc::Int(loc.var.id));
                }
                (&TLocKind::Var, _) => unreachable!(),
                (&TLocKind::Arr(ref _index), _) => {}
                (&TLocKind::Field(offset), _) => {
                    gen(BasicLoc::Field(loc.var.id, offset));
                }
            }
        }
        Write(ref _expr) => {},
        Test(ref _expr) => {},
        Skip(ref _name) => {},
    }
}

/// Which variables are no more faint after the given action.
/// 
/// Variables are no longer faint if they are used in a test or write, or
/// if they are used to compute a value for a non-faint variable.
///
/// ```
/// kill(p->n, [int[n] A]) = Ø
/// kill(p->n, [int x]) = Ø
/// kill(p->n, [{int fst; int snd} R]) = Ø
/// 
/// kill(p->n, [A[index] := expr]) = 
///     FV(index) U FV(expr) if A is not faint. Ø otherwise
/// kill(p->n, [x := expr]) = FV(expr) if x is not faint. Ø otherwise
/// kill(p->n, [R.fst := expr]) = FV(expr) if R.fst is not faint. Ø otherwise
/// kill(p->n, [R.snd := expr]) = FV(expr) if R.snd is not faint. Ø otherwise
/// kill(p->n, [R := (a1, a2)]) =
///   FV(fst) if R.fst is not faint else Ø U
///   FV(snd) if R.snd is not faint else Ø 
///
/// kill(p->n, [read x]) = Ø
/// kill(p->n, [read A[index]]) = FV(index) if A is not faint. Ø otherwise
/// kill(p->n, [read R.field]) = Ø
/// kill(p->n, [write expr]) = FV(expr)
/// kill(p->n, [skip]) = Ø
///
/// kill(p->n, [b]) = FV(b)
/// ```
fn kill_with<F: FnMut(BasicLoc)>(action: &BasicAction, 
    faint_vars: &FvState, mut kill: F
) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref _dec) => {}
        Assignment(ref loc, ref expr) => {
            match loc.kind {
                TLocKind::Var => {
                    let l = BasicLoc::Int(loc.var.id);
                    if ! faint_vars.contains(&l) {
                        free_vars_expr_with(expr, &mut kill);
                    }
                }
                TLocKind::Arr(ref index) => {
                    let l = BasicLoc::IntArr(loc.var.id);
                    if ! faint_vars.contains(&l) {
                        free_vars_expr_with(index, &mut kill);
                        free_vars_expr_with(expr, &mut kill);
                    }
                },
                TLocKind::Field(offset) => {
                    let l = BasicLoc::Field(loc.var.id, offset);
                    if ! faint_vars.contains(&l) {
                        free_vars_expr_with(expr, &mut kill);
                    }
                },
            }
        }
        Read(ref loc) => {
            match loc.kind {
                TLocKind::Arr(ref index) => {
                    let l = BasicLoc::IntArr(loc.var.id);
                    if ! faint_vars.contains(&l) {
                        free_vars_expr_with(index, &mut kill);
                    }
                }
                _ => {}
            }
        }
        Write(ref expr) => {
            free_vars_expr_with(expr, &mut kill);
        }
        Skip(_) => {},
        Test(ref expr) => {
            free_vars_expr_with(expr, &mut kill);
        }
    }
}

/// The faint variables at a single point in the program.
pub type FvState = HashSet<BasicLoc>;

pub struct FaintVariables {
    transfer: HashSet<BasicLoc>,
}

impl FaintVariables {
    pub fn new(transfer_set_capacity: usize) -> FaintVariables {
        FaintVariables {
            transfer: HashSet::with_capacity(transfer_set_capacity)
        }
    }
}

impl Analysis for FaintVariables {
    type State = FvState;
    
    #[inline]
    fn empty_state() -> Self::State {
        HashSet::new()
    }
    
    #[inline]
    fn initial_state(_start_id: VertexId, program: &Prograph) -> Self::State {
        let mut state = HashSet::new();
        state.extend(program.basic_locations());
        state
    }
    
    #[inline]
    fn is_forward_analysis() -> bool {
        false
    }
    
    fn transfer(&mut self, src: &mut Self::State, edge: &ProgEdge, dst: &mut Self::State, is_first_visit: bool, program: &Prograph) -> bool {
        let mut updated = is_first_visit;
        let action = edge.label;      
        self.transfer.clear();  
        // Transfer variables from the previous state
        
        // First time we transfer unconditionally
        if is_first_visit {
            for &loc in src.iter() {
                println!("  Transferring {}", program.name(loc));
                dst.insert(loc);
            }
            
            gen_with(action, |loc| {
                println!("  Generating {}", program.name(loc));
                dst.insert(loc);
            });
            
            kill_with(action, &src, |loc| {
                println!("  Killing {}", program.name(loc));
                dst.remove(&loc);
            });
        
        // Second time we find the intersection by filtering based on the old state.
        } else {
            for &loc in src.iter() {
                println!("  Transferring {}", program.name(loc));
                if dst.contains(&loc) {
                    self.transfer.insert(loc);
                }
            }
            
            gen_with(action, |loc| {
                println!("  Generating {}", program.name(loc));
                if dst.contains(&loc) {
                    self.transfer.insert(loc);
                }
            });
            
            kill_with(action, &src, |loc| {
                println!("  Killing {}", program.name(loc));
                self.transfer.remove(&loc);
            });
            
            // Faint variables are only ever killed
            assert!(self.transfer.len() <= dst.len());
            
            if dst.len() != self.transfer.len() {
                updated = true;
                mem::swap(dst, &mut self.transfer);
            }
        }
        
        updated
    }
}
