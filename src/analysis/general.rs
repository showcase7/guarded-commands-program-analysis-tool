use analysis::{Prograph, ProgEdge, VertexId};

use std::collections::VecDeque;
use std::iter;
use std::borrow::Cow;
use std::mem;

pub trait WorkList<'graph> {
    /// Initializes the work list for the current analysis.
    fn init<A: Analysis>(&mut self, program: &'graph Prograph, analysis: &A, start_id: VertexId);
    
    /// Inserts a new edge to be examined into the work list.
    fn insert(&mut self, edge: ProgEdge<'graph>);
    
    /// Removes the next edge to be examined from the work list and returns it.
    fn pop_next(&mut self) -> Option<ProgEdge<'graph>>;
}



pub trait Analysis : Sized {
    /// The information that this algorithm collects for each point in the
    /// program, such as the set of reaching definitions, live variables,
    /// faint variables etc.
    type State;
    
    /// Returns the 'empty' information state for each state in the domain.
    /// 
    /// Such as "No definitions reach this point" => Ø.
    fn empty_state() -> Self::State;
    
    /// How the first state should be initialized by default.
    fn initial_state(start_id: VertexId, program: &Prograph) -> Self::State;
    
    /// Whether this analysis traverses the program graph in forward or 
    /// backward direction (opposite the edge direction).
    fn is_forward_analysis() -> bool;
    
    /// Transfers information from one state to the next through an edge, and
    /// returns whether this changed the destination state.
    /// 
    /// This is where the items based on the Kill and Gen sets for the
    /// bit-vector framework are transferred.
    fn transfer<'a>(&mut self, src: &mut Self::State, edge: &ProgEdge<'a>, dst: &mut Self::State, is_first_visit: bool, program: &Prograph) -> bool;
    
    /// Runs the analysis using the provided work list.
    fn run<'a, W: WorkList<'a>, F: FnMut(&mut Self::State, &mut W)>(
        &mut self,
        program: &'a Prograph, 
        work_list: &mut W,
        start_id: VertexId,
        mut initializer: F
    ) -> Vec<Self::State> {
        
        work_list.init(program, self, start_id);
        
        let mut transfer_count = 0;
        
        let mut dom = iter::repeat(())
            .take(program.num_vertices())
            .map(|_| Self::empty_state())
            .collect::<Vec<_>>();
        
        let mut visited = iter::repeat(false)
            .take(program.num_vertices())
            .collect::<Vec<_>>();
        
        let is_forward_analysis = Self::is_forward_analysis();
    
        for edge in program.neighbors(start_id) {
            if edge.dir.is_out() == is_forward_analysis {
                work_list.insert(edge);
            }
        }
        
        dom[start_id] = Self::initial_state(start_id, program);
        visited[start_id] = true;
        initializer(&mut dom[start_id], work_list);
    
        //println!("Work list:");
        //println!("{:#?}", work_list);
    
    
        while let Some(edge) = work_list.pop_next() {
            println!("Checking edge {}: [{} -> {}]", edge.id, edge.src, edge.dst);
            // Update dst with the defs from src
            //if ! defs.contain
            // Make sure it's okay to move from one place to another
            if edge.src == edge.dst {
                continue; // Nothing new here anyway
            }
            let (src, dst) = if is_forward_analysis {
                (edge.src, edge.dst)
            } else {
                (edge.dst, edge.src)
            };
            println!("  Transferring from {}->{}", src, dst);
            let prev = unsafe { ((&mut dom[src]) as *mut Self::State).as_mut().unwrap() };
            let cur = unsafe { ((&mut dom[dst]) as *mut Self::State).as_mut().unwrap() };
            
            let updated = self.transfer(prev, &edge, cur, !visited[dst], program);
            transfer_count += 1;
            visited[dst] = true;
        
            // Notify dependents if the size has changed
            if updated {
                for edge in program.neighbors(dst) {
                    if edge.dir.is_out() == is_forward_analysis {
                        work_list.insert(edge);
                    }
                }
            }
        }
        
        println!("Final transfer count: {}", transfer_count);
    
        dom
    }
}

/// Stack-based work list (LIFO).
pub struct StackWl<'graph> {
    pub stack: Vec<ProgEdge<'graph>>,
}

impl<'graph> StackWl<'graph> {
    pub fn new() -> StackWl<'graph> {
        StackWl {
            stack: Vec::new()
        }
    }
}

impl<'graph> WorkList<'graph> for StackWl<'graph> {
    #[inline]
    fn init<A: Analysis>(&mut self, _program: &'graph Prograph, _analysis: &A, _start_id: VertexId) {
        self.stack.clear();
    }
    
    #[inline]
    fn insert(&mut self, edge: ProgEdge<'graph>) {
        self.stack.push(edge);
    }
    
    #[inline]
    fn pop_next(&mut self) -> Option<ProgEdge<'graph>> {
        self.stack.pop()
    }
}

/// Queue-based work list (FIFO).
pub struct QueueWl<'graph> {
    pub queue: VecDeque<ProgEdge<'graph>>,
}

impl<'graph> QueueWl<'graph> {
    pub fn new() -> QueueWl<'graph> {
        QueueWl {
            queue: VecDeque::new()
        }
    }
}

impl<'graph> WorkList<'graph> for QueueWl<'graph> {
    #[inline]
    fn init<A: Analysis>(&mut self, _program: &'graph Prograph, _analysis: &A, _start_id: VertexId) {
        self.queue.clear();
    }
    
    #[inline]
    fn insert(&mut self, edge: ProgEdge<'graph>) {
        self.queue.push_back(edge);
    }
    
    #[inline]
    fn pop_next(&mut self) -> Option<ProgEdge<'graph>> {
        self.queue.pop_front()
    }
}

pub type VertexOrderMap = Vec<usize>;

/// Reverse post-order work ordering.
pub struct RevPostWl<'graph> {
    program: Cow<'graph, Prograph>,
    ordering: VertexOrderMap,
    current: Vec<ProgEdge<'graph>>,
    pending: Vec<ProgEdge<'graph>>
}

impl<'graph> RevPostWl<'graph> {
    pub fn new() -> RevPostWl<'graph> {
        RevPostWl {
            program: Cow::Owned(Prograph::empty()),
            ordering: Vec::new(),
            current: Vec::new(),
            pending: Vec::new(),
        }
    }
}

/*
Edge categorisation for the reverse post-order algorithm
Categories:
    Tree edges:     edges present in the spanning forest.
    Forward edges:  edges that are not tree edges and that go from a
                    node to a proper descendant in the tree.
    Back edges:     edges that go from descendants to ancestors (including
                    self-loops).
    Cross edges:    edges that go between nodes that are unrelated by the
                    ancestor and descendant relations.

Reverse postorder (rPostorder) topologically sorts tree edges as well as the
forward and cross edges.

Tree edges: Stored from the DFS
Forward edges: Can I use a Fibonacci tree walk here? Or do I just walk back
through the DFSF starting at the end vertex?

Waitasec. This sounds like LCA. 
  For forward edges, the LCA will be the 'source node' of the edge.
  For cross nodes, the LCA will be some other node.

*/

fn reverse_postorder(program: &Prograph, start: VertexId, is_forward: bool) -> VertexOrderMap {    
    fn post_order_into(program: &Prograph, vertex: VertexId, is_forward: bool,
        visited: &mut Vec<bool>, order: &mut VertexOrderMap, next_id: &mut usize,
    ) {
        visited[vertex] = true;
        for edge in program.neighbors(vertex) {
            let next_vertex = match (is_forward, edge.dir.is_out()) {
                (true, true) => {
                    edge.dst // Catch the dst of the outgoing edge
                }
                (false, false) => {
                    edge.src // Catch the source of the ingoing edge
                }
                _ => continue,
            };
            if ! visited[next_vertex] {
                post_order_into(program, next_vertex, is_forward, visited, order, next_id);
            }
        }
        let id = *next_id;
        *next_id = next_id.wrapping_sub(1);
        order[vertex] = id;
    }
    
    let mut order = iter::repeat(0)
        .take(program.num_vertices())
        .collect::<Vec<_>>();
    
    let mut visited = iter::repeat(false)
        .take(program.num_vertices())
        .collect::<Vec<_>>();
    
    let mut next_id = program.num_vertices().wrapping_sub(1);
    post_order_into(program, start, is_forward, &mut visited, &mut order, &mut next_id);
    println!("Reverse post order starting at {}, forward: {}", start, is_forward);
    order
}

/*
BIG PROBLEM

I'm currently considering each edge at a time, rather than each 'constraint'.
This means that the algorithm won't really work, afaict...
Since it is about evaluating the constraint each... owait
If I 'just' order the edges by destination and vertexId... that'll work!

*/

impl<'graph> WorkList<'graph> for RevPostWl<'graph> {
    #[inline]
    fn init<A: Analysis>(&mut self, program: &'graph Prograph, _analysis: &A, start_id: VertexId) {
        self.program = Cow::Borrowed(program);
        
        // do a DFS based on the flow direction of the analysis
        self.ordering = reverse_postorder(program, start_id, A::is_forward_analysis());
    }
    
    #[inline]
    fn insert(&mut self, edge: ProgEdge<'graph>) {
        println!("Inserting Edge {}->{}", edge.src, edge.dst);
        self.pending.push(edge);
    }
    
    #[inline]
    fn pop_next(&mut self) -> Option<ProgEdge<'graph>> {
        let cur_empty = self.current.is_empty();
        if cur_empty {
            let pen_empty = self.pending.is_empty();
            if pen_empty {
                None
            } else {
                // Swap the current and the pending lists.
                mem::swap(&mut self.current, &mut self.pending);
                self.pending.clear();
                
                // Swap the pointer to self.ordering so that I can borrow the
                // ordering immutably inside the closure.
                let mut ext_ord = Vec::new();
                mem::swap(&mut ext_ord, &mut self.ordering);
                self.current.sort_by_key(|edge| {
                    // Sort first by constraint (place), then by unique ID
                    (ext_ord[edge.dst], edge.id)
                });
                mem::swap(&mut ext_ord, &mut self.ordering);
                //println!("Swapping current and pending, cur: {:#?}", self.current);
                
                self.current.pop()
            }
        } else {
            self.current.pop()
        }
    }
}

pub struct RoundRobinWl<'graph> {
    /// The incoming edges of a reverse post-order ordering of the 
    /// program states in the given program (constraints).
    order: Vec<ProgEdge<'graph>>,
    
    /// The edge currently being applied
    current_index: usize,
    
    /// Whether any of the transfers updated the state since the round began.
    updated: bool,
}

impl<'graph> RoundRobinWl<'graph> {
    pub fn new() -> RoundRobinWl<'graph> {
        RoundRobinWl {
            order: Vec::new(),
            current_index: 0,
            updated: false,
        }
    }
}

fn vertex_visit_order(order_map: &VertexOrderMap, num_vertices: usize) -> Vec<VertexId> {
    // Now get a list of vertices from that 
    let mut order = (0..num_vertices).collect::<Vec<_>>();
    order.sort_by_key(|&id| order_map[id]);
    order
}

impl<'graph> WorkList<'graph> for RoundRobinWl<'graph> {
    /// Initializes the work list for the current analysis.
    fn init<A: Analysis>(&mut self, program: &'graph Prograph, _analysis: &A, start_id: VertexId) {
        // Reset the work list
        self.current_index = 0;
        self.updated = false;
        self.order.clear();
        
        // Get a map from each vertex to its ordering
        let state_order_map = reverse_postorder(program, start_id, A::is_forward_analysis());
        
        // Get an ordered list of constraints to update
        let order = vertex_visit_order(&state_order_map, program.num_vertices());
        
        let is_forward = A::is_forward_analysis();
        println!("Preparing RevPost edge ordering...");
        for vertex in order {
            println!("  Constraint: {}", vertex);
            for edge in program.neighbors(vertex) {
                println!("    Checking edge {}: {}", edge.id, edge.fmt());
                // If this is a forward analysis, it is into the 'dst' end
                // of the edge
                if is_forward {
                    if edge.dir.is_in() {
                        println!("  Adding edge {}", edge.fmt());
                        self.order.push(edge);
                    }
                
                // If this is a backward analysis, it is into the 'src' end
                // of the edge
                } else {
                    if edge.dir.is_out() {
                        println!("  Adding edge {}", edge.fmt());
                        self.order.push(edge);
                    }
                }
            }
        }
    }
    
    /// Inserts a new edge to be examined into the work list.
    fn insert(&mut self, _edge: ProgEdge<'graph>) {
        println!("  Update scheduled!");
        self.updated = true;
    }
    
    /// Removes the next edge to be examined from the work list and returns it.
    fn pop_next(&mut self) -> Option<ProgEdge<'graph>> {
        if self.current_index == self.order.len() {
            if self.updated {
                self.current_index = 0;
                self.updated = false;
            } else {
                return None;
            }
        }
        let index = self.current_index;
        self.current_index += 1;
        Some(self.order[index].clone())
    }
}





