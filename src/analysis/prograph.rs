//! Module with basic types and functions used for program analysis.

use ast::*;

use std::collections::{HashMap, BTreeMap};
use std::fmt::Debug;
use std::usize;
use std::rc::Rc;

// Always use this behind an RC?
/// The basic actions of the MicroC language.
///
/// The basic actions are the smallest set of actions that can model the
/// state and execution of a MicroC program.
#[derive(Debug, Clone)]
pub enum BasicAction {
    Declaration(TDec),
    Assignment(TLoc, TExpr),
    Test(TExpr),
    Read(TLoc),
    Write(TExpr),
    Skip(String),
}

impl BasicAction {
    pub fn format(&self, var_names: &[String]) -> String {
        let mut s = String::new();
        self.format_into(var_names, &mut s);
        s
    }
    
    fn format_loc_into(&self, var_names: &[String], loc: &TLoc, s: &mut String) {
        match loc.kind {
            TLocKind::Var => {
                s.push_str(&var_names[loc.var.id]);
            }
            TLocKind::Arr(ref expr) => {
                s.push_str(&var_names[loc.var.id]);
                s.push('[');
                self.format_expr_into(var_names, expr, s);
                s.push(']');
            }
            TLocKind::Field(offset) => {
                s.push_str(&var_names[loc.var.id]);
                match offset {
                    0 => s.push_str("fst"),
                    1 => s.push_str("snd"),
                    _ => unimplemented!(),
                }
            }
        }
    }
    
    fn format_expr_into(&self, var_names: &[String], expr: &TExpr, s: &mut String) {
        match expr.kind {
            TExprKind::Int(value) => {
                s.push_str(&value.to_string());
            },
            TExprKind::Access(ref loc) => {
                self.format_loc_into(var_names, loc, s);
            },
            TExprKind::Bool(value) => {
                s.push_str(if value { "true" } else { "false" });
            },
            TExprKind::Binary(ref left, op, ref right) => {
                self.format_expr_into(var_names, left, s);
                s.push(' ');
                s.push_str(match op {
                    BinOp::Plus => "+",
                    BinOp::Minus => "-",
                    BinOp::Times => "*",
                    BinOp::Div => "/",
                    BinOp::Rem => "%",
                    BinOp::Lt => "<",
                    BinOp::LtEq => "<=",
                    BinOp::Gt => ">",
                    BinOp::GtEq => ">=",
                    BinOp::Eq => "==",
                    BinOp::NotEq => "!=",
                    BinOp::And => "&&",
                    BinOp::Or => "||",
                });
                s.push(' ');
                self.format_expr_into(var_names, right, s);
            },
            TExprKind::Unary(op, ref expr) => {
                s.push_str(match op {
                    UnOp::Not => "not",
                });
                s.push(' ');
                self.format_expr_into(var_names, expr, s);
            },
            TExprKind::Record(ref fst, ref snd) => {
                s.push_str("( ");
                self.format_expr_into(var_names, fst, s);
                s.push_str(", ");
                self.format_expr_into(var_names, snd, s);
                s.push(')');
            },
        }
    }
    
    pub fn format_into(&self, var_names: &[String], s: &mut String) {
        use self::BasicAction::*;
        
        match *self {
            Declaration(ref dec) => {
                match dec.kind {
                    TDecKind::Int => {
                        s.push_str("int ");
                        s.push_str(&var_names[dec.var.id]);
                    }
                    TDecKind::IntArr(size) => {
                        s.push_str("int[");
                        s.push_str(&size.to_string());
                        s.push_str("] ");
                        s.push_str(&var_names[dec.var.id]);
                    }
                    TDecKind::Record => {
                        s.push_str("{int fst; int snd} ");
                        s.push_str(&var_names[dec.var.id]);
                    }
                }
            }
            
            Assignment(ref loc, ref expr) => {
                self.format_loc_into(var_names, loc, s);
                s.push_str(" := ");
                self.format_expr_into(var_names, expr, s);
            }
            Test(ref expr) => {
                self.format_expr_into(var_names, expr, s);
            },
            Read(ref loc) => {
                s.push_str("stdin ? ");
                self.format_loc_into(var_names, loc, s);
            }
            Write(ref expr) => {
                s.push_str("stdout ! ");
                self.format_expr_into(var_names, expr, s);
            },
            Skip(ref label) => {
                if label != "" {
                    s.push_str(label);
                } else {
                    s.push_str("skip");
                }
            }
        }
    }
}

/// The basic locations of the MicroC language.
///
/// A basic location is a memory location that can be followed statically.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Ord, Eq, Hash)]
pub enum BasicLoc {
    /// A variable containing an integer.
    Int(VarId),
    
    /// A variable containing an array of integers.
    IntArr(VarId),
    
    /// A record field at the given offset inside the record.
    Field(VarId, usize),
}

impl BasicLoc {
    pub fn format(&self, var_names: &[String]) -> String {
        let mut s = String::new();
        self.format_into(var_names, &mut s);
        s
    }
    
    pub fn format_into(&self, var_names: &[String], s: &mut String) {
        match *self {
            BasicLoc::Int(var) => {
                s.push_str(&var_names[var]);
            }
            BasicLoc::IntArr(var) => {
                s.push_str(&var_names[var]);
            }
            BasicLoc::Field(var, offset) => {
                s.push_str(&var_names[var]);
                s.push('.');
                s.push_str(match offset {
                    0 => "fst",
                    1 => "snd",
                    _ => unimplemented!(),
                });
            }
        }
    }
}

/// Identifies a vertex inside a graph.
pub type VertexId = usize;

/// Identifies an edge inside a graph.
pub type EdgeId = isize;

/// The direction of an edge
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum EdgeDir {
    In,
    Out,
}
impl EdgeDir {
    #[inline]
    pub fn is_out(&self) -> bool {
        match *self {
            EdgeDir::In => false,
            EdgeDir::Out => true,
        }
    }
    
    #[inline]
    pub fn is_in(&self) -> bool {
        match *self {
            EdgeDir::In => true,
            EdgeDir::Out => false,
        }
    }
}

/// A directed edge in a graph.
#[derive(Debug, Clone)]
pub struct Edge<'a, T: 'a> {
    /// The origin of the edge.
    pub src: VertexId, 
    
    /// The destination of the edge.
    pub dst: VertexId, 
    
    /// Whether this edge goes into this vertex or out of it.
    pub dir: EdgeDir,
    
    /// Information associated with this edge.
    pub label: &'a T, 
    
    /// A unique identifier of this edge inside its graph.
    pub id: EdgeId,
}

impl<'a, T> Edge<'a, T> {
    /// Creates a new edge.
    #[inline]
    pub fn new(src: VertexId, dir: EdgeDir, dst: VertexId, label: &'a T, id: EdgeId) -> Edge<'a, T> {
        Edge {
            src, dir, dst, label, id,
        }
    }
    
    pub fn fmt(&self) -> String {
        format!("{}->{}", self.src, self.dst)
    }
}

/// The edge type for program graphs.
pub type ProgEdge<'a> = Edge<'a, Rc<BasicAction>>;

/// A program graph; a simplified graph that fully describes the execution of
/// a program inside the current analysis domain. The graph has a basic action
/// associated with each edge, that is either executed when that edge is taken 
/// or describes a condition for taking that edge.
///
/// The program graph also contains a set of basic locations that are places
/// in memory that can be statically identified and therefore analysed.
#[derive(Debug, Clone)]
pub struct Prograph {
    graph: Digraph<Rc<BasicAction>>,
    basic_locations: Vec<BasicLoc>,
    var_names: Vec<String>,
}

impl Prograph {
    /// Creates a new program graph from the given typed program.
    pub fn new(program: &TProgram) -> Prograph {
        let mut graph = Digraph::new();
        let start = graph.add_vertex();
        let end = graph.add_vertex();
        let mut locs = Vec::new();
    
        // Add the declarations.
        let mut prev = start;
        for dec in program.declarations.iter() {
            // Add the basic location
            match dec.kind {
                TDecKind::Int => {
                    locs.push(BasicLoc::Int(dec.var.id));
                }
                TDecKind::IntArr(_) => {
                    locs.push(BasicLoc::IntArr(dec.var.id));
                }
                TDecKind::Record => {
                    locs.push(BasicLoc::Field(dec.var.id, 0));
                    locs.push(BasicLoc::Field(dec.var.id, 1));
                }
            }
            // Add the edge
            let action = Rc::new(BasicAction::Declaration(dec.clone()));
            let new = graph.add_vertex();
            graph.add_edge(prev, new, action);
            prev = new;
        }
    
        // Add the statements
        let breakp = usize::MAX;
        connect_body(&mut graph, prev, &program.statements, end, breakp);
    
        Prograph {
            graph: graph,
            basic_locations: locs,
            var_names: program.var_names.clone(),
        }
    }
    
    /// Creates an empty program graph.
    pub fn empty() -> Prograph {
        Prograph {
            graph: Digraph::new(),
            basic_locations: Vec::new(),
            var_names: Vec::new(),
        }
    }
    
    #[inline]
    pub fn edge(&self, id: EdgeId) -> Option<&(VertexId, VertexId)> {
        self.graph.edge_lookup.get(id as usize)
    }
    
    pub fn format_edge(&self, id: EdgeId) -> String {
        let mut s = String::new();
        let &(src, dst) = self.edge(id).expect("Invalid edge id!");
        if src == usize::MAX {
            s.push('?');
        } else {
            s.push_str(&src.to_string());
        }
        s.push('→');
        s.push_str(&dst.to_string());
        s
    }
    
    pub fn to_dot(&self) -> String {
        let mut s = String::new();
        self.graph.format_to_dot(&mut s, |action| {
            action.format(&self.var_names)
        });
        s
    }
    
    pub fn basic_locations(&self) -> &Vec<BasicLoc> {
        &self.basic_locations
    }
    
    /// Returns the name of the given basic location.
    pub fn name(&self, loc: BasicLoc) -> String {
        match loc {
            BasicLoc::Int(var) => {
                self.var_names[var].clone()
            }
            BasicLoc::IntArr(var) => {
                self.var_names[var].clone()
            }
            BasicLoc::Field(var, field) => {
                format!("{}.{}", 
                    self.var_names[var], 
                    if field == 0 { "fst" } else { "snd" }
                )
            }
        }
    }
    
    pub fn num_vertices(&self) -> usize {
        self.graph.num_vertices
    }
    
    pub fn var_names(&self) -> &Vec<String> {
        &self.var_names
    }
    
    pub fn neighbors<'a>(&'a self, from: VertexId) -> impl Iterator<Item=ProgEdge<'a>> {
        self.graph.neighbors(from)
    }
    
    pub fn edges<'a>(&'a self) -> impl Iterator<Item=ProgEdge<'a>> {
        self.graph.edges()
    }
}

/// A Directed graph.
///
/// The graph contains internal back-edges, so it is possible to search the
/// graph in both directions.
#[derive(Debug, Clone)]
pub struct Digraph<T: Clone + Debug> {
    pub edges: HashMap<VertexId, BTreeMap<(VertexId, EdgeId), (EdgeDir, T)>>,
    pub num_vertices: usize,
    pub next_edge_id: EdgeId,
    pub edge_lookup: Vec<(VertexId, VertexId)>,
}

impl<T: Clone + Debug> Digraph<T> {
    pub fn new() -> Digraph<T> {
        Digraph {
            edges: HashMap::new(),
            num_vertices: 0,
            // ID 0 is reserved for ?->0
            next_edge_id: 1,
            edge_lookup: vec![(usize::MAX, 0)],
        }
    }
    
    pub fn add_vertex(&mut self) -> VertexId {
        let id = self.num_vertices;
        self.num_vertices += 1;
        id
    }
    
    #[inline]
    pub fn gen_edge_id(&mut self) -> EdgeId {
        let id = self.next_edge_id;
        self.next_edge_id += 1;
        id
    }
    
    pub fn add_edge(&mut self, from: VertexId, to: VertexId, value: T) {        
        let edge_id = self.gen_edge_id();
        self.edges.entry(from)
            .or_insert_with(BTreeMap::new)
            .insert((to, edge_id), (EdgeDir::Out, value.clone()));
        
        self.edges.entry(to)
            .or_insert_with(BTreeMap::new)
            .insert((from, edge_id), (EdgeDir::In, value));
        
        assert!(edge_id == self.edge_lookup.len() as EdgeId);
        self.edge_lookup.push((from, to));
    }
    
    pub fn neighbors<'a>(&'a self, src: VertexId) -> impl Iterator<Item=Edge<'a, T>> {
        self.edges[&src].iter().map(move |(&(dst, eid), &(dir, ref label))| {
            if dir.is_out() {
                Edge::new(src, dir, dst, label, eid)
            } else {
                Edge::new(dst, dir, src, label, eid)
            }
        })
    }
    
    pub fn edges<'a>(&'a self) -> impl Iterator<Item=Edge<'a, T>> {
        self.edges.iter().flat_map(|(&src, conns)| {
            conns.iter().map(move |(&(dst, eid), &(dir, ref label))| {
                if dir.is_out() {
                    Edge::new(src, dir, dst, label, eid)
                } else {
                    Edge::new(dst, dir, src, label, eid)
                }
            })
        })
    }
    
    pub fn format_to_dot<F: FnMut(&T) -> String>(&self, s: &mut String, mut f: F) {
        s.push_str("digraph {\n");
        //s.push_str("  forcelabels = true;\n");
        s.push_str("  \"_\" [shape=none] [label=\"\"];\n");
        for i in 0..self.num_vertices {
            match i {
                1 => {
                    s.push_str(&format!("  \"{}\" [shape=doublecircle];\n", i));
                }
                _ => {
                    s.push_str(&format!("  \"{}\";\n", i));
                }
            }
        }
        
        s.push_str("  \"_\" -> \"0\";\n");
        
        for (&src, conns) in self.edges.iter() {
            for (&(dst, _), &(dir, ref value)) in conns.iter() {
                if ! dir.is_out() {
                    continue;
                }
                s.push_str(&format!("  \"{}\" -> \"{}\"", src, dst));
                let label = f(value);
                if label.as_str() != "" {
                    s.push_str(&format!(" [label=\" {} \"];\n", label));
                } else {
                    s.push_str(";\n");
                }
            }
        }
        
        s.push_str("}\n");
    }
}

/*impl<'a, T: 'a + Clone> Iterator for Edges<'a, T> {
    type Item = (VertexId, bool, VertexId, &'a T, EdgeId);
}*/

pub fn negate_expr(expr: &TExpr) -> TExpr {
    use ast::TExprKind::*;
    
    let kind = match expr.kind {
        Bool(value) => Bool(!value),
        Binary(ref left, op, ref right) => {
            use ast::BinOp::*;
        
            let new_op = match op {
                Lt => GtEq,
                LtEq => Gt,
                Gt => LtEq,
                GtEq => Lt,
                Eq => NotEq,
                NotEq => Eq,
                // ...
                And => Or,
                Or => And,
                _ => unreachable!(),
            };
            
            match op {
                And | Or => {
                    Binary(Box::new(negate_expr(left)), new_op, Box::new(negate_expr(right)))
                }
                _ => {
                    Binary(left.clone(), new_op, right.clone())
                }
            }
        },
        Unary(UnOp::Not, ref expr) => {
            expr.kind.clone()
        },
        Int(_) | Access(_) | Record(_, _) => {
            unreachable!();
        }
    };
    TExpr {
        kind: kind,
        typ: expr.typ,
        span: expr.span,
    }
}


fn connect(graph: &mut Digraph<Rc<BasicAction>>, prev: VertexId, stmt: &TStmt, next: VertexId, breakp: VertexId) {
    use ast::TStmtKind::*;
    
    match stmt.kind {
        Assign(ref loc, ref expr) => {
            let action = Rc::new(BasicAction::Assignment(loc.clone(), expr.clone()));
            graph.add_edge(prev, next, action);
        },
        If(ref cond, ref body, ref else_body) => {
            
            let pos_test = Rc::new(BasicAction::Test(cond.clone()));
            let neg_test = Rc::new(BasicAction::Test(negate_expr(cond)));
            
            // Positive test body
            let pos = graph.add_vertex();
            graph.add_edge(prev, pos, pos_test);
            connect_body(graph, pos, body, next, breakp);
            
            // Negative test
            if let Some(else_body) = else_body.as_ref() {
                let neg = graph.add_vertex();
                graph.add_edge(prev, neg, neg_test);
                connect_body(graph, neg, else_body, next, breakp);
            } else {
                graph.add_edge(prev, next, neg_test);
            }
        },
        While(ref cond, ref body) => {
            let first = graph.add_vertex();
            
            // Positive condition destination
            let cond_action = Rc::new(BasicAction::Test(cond.clone()));
            graph.add_edge(prev, first, cond_action);
            
            // Negative condition destination
            let not_cond_action = Rc::new(BasicAction::Test(negate_expr(cond)));
            graph.add_edge(prev, next, not_cond_action);
            
            // Body and loop
            connect_body(graph, first, body, prev, next);
        },
        Break => {
            if breakp == usize::MAX {
                panic!("Invalid breakpoint vertex :c");
            }
            graph.add_edge(prev, breakp, Rc::new(BasicAction::Skip("break".into())));
        },
        Read(ref loc) => {
            let action = Rc::new(BasicAction::Read(loc.clone()));
            graph.add_edge(prev, next, action);
        },
        Write(ref expr) => {
            let action = Rc::new(BasicAction::Write(expr.clone()));
            graph.add_edge(prev, next, action);
        },
    }
}

fn connect_body(graph: &mut Digraph<Rc<BasicAction>>, mut prev: VertexId, statements: &Vec<TStmt>, next: VertexId, breakp: VertexId) {
    let last = if statements.is_empty() {
        0
    } else {
        statements.len() - 1
    };
    for (i, stmt) in statements.iter().enumerate() {
        let stmt_next = if i == last {
            next
        } else {
            graph.add_vertex()
        };
        //eprintln!("Body statement {}: stmt_next = {}", i, stmt_next);
        connect(graph, prev, stmt, stmt_next, breakp);
        prev = stmt_next;
    }
}

