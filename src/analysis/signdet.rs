//! Module for the sign detection analysis.
//!
//! This analysis finds the possible signs {-,0,+} of each basic location.

use analysis::{Analysis, BasicLoc, BasicAction, VertexId, Prograph, ProgEdge};
use analysis::prograph::negate_expr;
use ast::*;
use std::collections::HashMap;
use std::ops::{Add, Sub, AddAssign, SubAssign};
use std::fmt;
use std::usize;

const MINUS_FLAG: u8    = 0b0000_0100;
const ZERO_FLAG: u8     = 0b0000_0010;
const PLUS_FLAG: u8     = 0b0000_0001;

/// A single sign.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Sign {
    Minus,
    Zero,
    Plus,
}

/// A set of signs
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Signs {
    flags: u8
}

impl Signs {
    #[inline]
    pub fn new() -> Signs {
        Signs { flags: 0 }
    }
    
    #[inline]
    pub fn all() -> Signs {
        Signs { flags: MINUS_FLAG | ZERO_FLAG | PLUS_FLAG }
    }
    
    #[inline]
    pub fn has_minus(&self) -> bool {
        (self.flags & MINUS_FLAG) != 0
    }
    
    #[inline]
    pub fn has_zero(&self) -> bool {
        (self.flags & ZERO_FLAG) != 0
    }
    
    #[inline]
    pub fn has_plus(&self) -> bool {
        (self.flags & PLUS_FLAG) != 0
    }
    
    pub fn signs(&self) -> &[Sign] {
        use self::Sign::*;
        
        match self.flags {
            0b0000_0000 => &[],
            0b0000_0100 => &[Minus],
            0b0000_0110 => &[Minus, Zero],
            0b0000_0111 => &[Minus, Zero, Plus],
            0b0000_0010 => &[Zero],
            0b0000_0011 => &[Zero, Plus],
            0b0000_0001 => &[Plus],
            _ => unreachable!(),
        }
    }
    
    #[inline]
    pub fn minus() -> Signs {
        Signs { flags: MINUS_FLAG }
    }
    
    #[inline]
    pub fn zero() -> Signs {
        Signs { flags: ZERO_FLAG }
    }
    
    #[inline]
    pub fn plus() -> Signs {
        Signs { flags: PLUS_FLAG }
    }
    
    #[inline]
    pub fn add_sign(&mut self, sign: Sign) {
        match sign {
            Sign::Minus => self.flags |= MINUS_FLAG,
            Sign::Zero  => self.flags |= ZERO_FLAG,
            Sign::Plus  => self.flags |= PLUS_FLAG,
        }
    }
    
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.flags == 0
    }
}

impl fmt::Display for Signs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{")?;
        let mut flag_count = 0;
        if self.has_minus() {
            flag_count += 1;
        }
        if self.has_zero() {
            flag_count += 1;
        }
        if self.has_plus() {
            flag_count += 1;
        }
        let mut written = 0;
        if self.has_minus() {
            write!(f, "-")?;
            written += 1;
            if written < flag_count {
                write!(f, ",")?;
            }
        }
        if self.has_zero() {
            write!(f, "0")?;
            written += 1;
            if written < flag_count {
                write!(f, ",")?;
            }
        }
        if self.has_plus() {
            write!(f, "+")?;
        }
        write!(f, "}}")
    }
}

impl Add for Signs {
    type Output = Signs;

    #[inline]
    fn add(self, other: Signs) -> Signs {
        Signs { flags: self.flags | other.flags }
    }
}

impl Sub for Signs {
    type Output = Signs;

    #[inline]
    fn sub(self, other: Signs) -> Signs {
        // Remove the shared flags.
        Signs { flags: self.flags - (self.flags & other.flags) }
    }
}

impl AddAssign for Signs {
    fn add_assign(&mut self, rhs: Signs) {
        *self = self.add(rhs);
    }
}

impl SubAssign for Signs {
    fn sub_assign(&mut self, rhs: Signs) {
        *self = self.sub(rhs);
    }
}


/// Calls a closure for each location contained by the given declaration.
fn visit_dec_locs<F: FnMut(BasicLoc)>(dec: &TDec, mut update: F) {
    match dec.kind {
        TDecKind::Int => {
            update(BasicLoc::Int(dec.var.id));
        },
        TDecKind::IntArr(_) => {
            update(BasicLoc::IntArr(dec.var.id));
        },
        TDecKind::Record => {
            update(BasicLoc::Field(dec.var.id, 0));
            update(BasicLoc::Field(dec.var.id, 1));
        },
    }
}

/// Returns the basic location that a location refers to.
fn visit_loc_locs<F: FnMut(BasicLoc)>(loc: &TLoc, mut update: F) {
    match loc.kind {
        TLocKind::Var => {
            match loc.typ {
                Type::Record => {
                    update(BasicLoc::Field(loc.var.id, 0));
                    update(BasicLoc::Field(loc.var.id, 1));
                }
                Type::IntArr(_) => {
                    update(BasicLoc::IntArr(loc.var.id));
                }
                Type::Int => {
                    update(BasicLoc::Int(loc.var.id));
                }
                _ => unreachable!(),
            }            
        },
        TLocKind::Arr(ref _index) => {
            update(BasicLoc::IntArr(loc.var.id));
        }    
        TLocKind::Field(offset) => {
            update(BasicLoc::Field(loc.var.id, offset));
        },
    }
}


/*
&&      intersection of signs
||      union of signs
==      same signs (can find dead code if both sides are 'fixed')
        Find out what signs the two can take
        Check each valid combination of these signs
            Find out whether each side can be constrained to that sign
            without contradicting the other side.
            ("x must be negative" vs "x must be positive" is a contradiction)
!=      works like '==', except the valid combinations must be 
        different signs.
<       less than
<=      less than or eq
>       greater than
>=      greater than or eq

The equality/inequality operators must return both signs and constraints?
So that they basically say 'for this to be true, this must be true'
....AM I SOLVING PREDICATE LOGIC, WHY YES, YES I AM :|

*/

/// Returns whether the two signs are a valid combination for the given relational operator.
fn valid_relational_signs(left: Sign, op: BinOp, right: Sign) -> bool {
    use self::Sign::*;
    
    match op {
        BinOp::Lt => { // '<'
            match (left, right) {
                (Minus, Minus)  => true,
                (Minus, Zero)   => true,
                (Minus, Plus)   => true,
                (Zero, Minus)   => false,
                (Zero, Zero)    => false,
                (Zero, Plus)    => true,
                (Plus, Minus)   => false,
                (Plus, Zero)    => false,
                (Plus, Plus)    => true,
            }
        },
        BinOp::LtEq => { // <=
            match (left, right) {
                (Minus, Minus)  => true,
                (Minus, Zero)   => true,
                (Minus, Plus)   => true,
                (Zero, Minus)   => false,
                (Zero, Zero)    => true,
                (Zero, Plus)    => true,
                (Plus, Minus)   => false,
                (Plus, Zero)    => false,
                (Plus, Plus)    => true,
            }
        },
        BinOp::Gt => { // >
            match (left, right) {
                (Minus, Minus)  => true,
                (Minus, Zero)   => false,
                (Minus, Plus)   => false,
                (Zero, Minus)   => true,
                (Zero, Zero)    => false,
                (Zero, Plus)    => false,
                (Plus, Minus)   => true,
                (Plus, Zero)    => true,
                (Plus, Plus)    => true,
            }
        },
        BinOp::GtEq => { // >=
            match (left, right) {
                (Minus, Minus)  => true,
                (Minus, Zero)   => false,
                (Minus, Plus)   => false,
                (Zero, Minus)   => true,
                (Zero, Zero)    => true,
                (Zero, Plus)    => false,
                (Plus, Minus)   => true,
                (Plus, Zero)    => true,
                (Plus, Plus)    => true,
            }
        },
        BinOp::Eq => {
            match (left, right) {
                (Minus, Minus)  => true,
                (Minus, Zero)   => false,
                (Minus, Plus)   => false,
                (Zero, Minus)   => false,
                (Zero, Zero)    => true,
                (Zero, Plus)    => false,
                (Plus, Minus)   => false,
                (Plus, Zero)    => false,
                (Plus, Plus)    => true,
            }
        },
        BinOp::NotEq => {
            match (left, right) {
                (Minus, Minus)  => false,
                (Minus, Zero)   => true,
                (Minus, Plus)   => true,
                (Zero, Minus)   => true,
                (Zero, Zero)    => false,
                (Zero, Plus)    => true,
                (Plus, Minus)   => true,
                (Plus, Zero)    => true,
                (Plus, Plus)    => false,
            }
        },
        // ...
        _ => panic!("Non-relational operator in 'valid_relational_signs'"),
    }
}

/// Returns the possible signs that the two sides may have.
fn possible_signs(env: &SdState, left: &TExpr, op: BinOp, right: &TExpr) -> Option<(Signs, Signs)> {
    let left_signs = single_expr_signs(env, left);
    let right_signs = single_expr_signs(env, right);
    let mut ls = Signs::new();
    let mut rs = Signs::new();
    // Check whether each combination of signs is valid for this operator.
    for &l in left_signs.signs() {
        for &r in right_signs.signs() {
            if valid_relational_signs(l, op, r) {
                ls.add_sign(l);
                rs.add_sign(r);
            }
        }
    }
    
    if ls.is_empty() || rs.is_empty() {
        eprintln!("Found self-contradicatory expression!");
        None
    } else {
        Some((ls, rs))
    }
}

fn expr_loc(expr: &TExpr) -> Option<BasicLoc> {
    match expr.kind {
        TExprKind::Access(ref loc) => {
            let mut bloc = BasicLoc::Int(usize::MAX);
            visit_loc_locs(loc, |loc| {
                bloc = loc;
            });
            assert!(bloc != BasicLoc::Int(usize::MAX));
            Some(bloc)
        },
        _ => None,
    }
}

// I'll implement an Independent Attribute Analysis.
// The other could be done at a later time and would be pretty nice.

fn constrain_into(expr: &TExpr, signs: Signs, constraints: &mut SdState) {
    eprintln!("    Attempting to constrain expr:");
    eprintln!("      signs: {}", signs);
    eprintln!("      expr: {:#?}", expr);
    if let Some(loc) = expr_loc(expr) {
        eprintln!("    Constraint added!");
        constraints.entry(loc).or_insert_with(Signs::new).add_assign(signs);
    } else {
        // Let's not try to constrain compound arithmetic expressions.
        // The approach here would be to isolate each basic location on one hand of the
        // expression, evaluate the signs of the other side then constrain based on that.
        // ie: 'a < b + 2' would isolate b as 'a - 2 < b', ie: constrain('b', >, 'a - 2')
    }
}

fn binary_bool_constraints_into(env: &SdState, left: &TExpr, op: BinOp, right: &TExpr, constraints: &mut SdState) {
    // Try to constrain the environment based on each side of the expression.
    if let Some((left_signs, right_signs)) = possible_signs(env, left, op, right) {
        eprintln!("    Possible signs: {} {:?} {}", left_signs, op, right_signs);
        constrain_into(left, left_signs, constraints);
        constrain_into(right, right_signs, constraints);
    }
    
    match op {
        // Constrain sub-comparisons
        BinOp::And | BinOp::Or => {
            bool_constraints_into(env, left, constraints);
            bool_constraints_into(env, right, constraints);
        }
        _ => {}
    }
    
}

/*
Hmmmmm

My problem is that I need to recursively find out possible sign constraints
And these need to be combined together to avoid false positives.
But my API is such that I want the update function to only be applied to
the top level.
But why don't I change the approach then, and just return a set of possible
constraints?
*/

fn bool_constraints_into(env: &SdState, expr: &TExpr, constraints: &mut SdState) {
    use ast::TExprKind::*;
    //println!("bool_constraints_into, Expr: {:#?}", expr);
    assert_eq!(expr.typ, Type::Bool);
    
    match expr.kind {
        Bool(_) => {},
        Binary(ref left, op, ref right) => {
            binary_bool_constraints_into(env, left, op, right, constraints);
        },
        Unary(UnOp::Not, ref expr) => {
            let negated = negate_expr(expr);
            bool_constraints_into(env, &negated, constraints);
        },
        _ => unreachable!(),
    }
}

/// Returns the possible valid boolean constraints of the given state.
///
/// Not precise, but captures all possibilities (and simpler to implement).
fn bool_constraints(env: &SdState, expr: &TExpr) -> SdState {
    let mut constraints = SdState::new();
    bool_constraints_into(env, expr, &mut constraints);
    constraints
}

pub enum ExprSigns {
    Integer(Signs),
    Record(Signs, Signs),
}

/*
    Plus,
    Minus,
    Times,
    Div,
    Rem,
    // ...
    Lt,
    LtEq,
    Gt,
    GtEq,
    Eq,
    NotEq,
    // ...
    And,
    Or,
*/

fn arith_signs(left: Sign, op: BinOp, right: Sign) -> Signs {
    use self::Sign::*;
    
    match op {
        BinOp::Plus => {
            match (left, right) {
                (Minus, Minus) => Signs::minus(),
                (Zero, Zero) => Signs::zero(),
                (Plus, Plus) => Signs::plus(),
                (Minus, Zero) | 
                (Zero, Minus) => Signs::minus(),
                (Minus, Plus) | 
                (Plus, Minus) => Signs::all(),
                (Zero, Plus)  | 
                (Plus, Zero)  => Signs::plus(),
            }
        }
        BinOp::Minus => {
            match (left, right) {
                (Minus, Minus)  => Signs::all(),
                (Minus, Zero)   => Signs::minus(),
                (Minus, Plus)   => Signs::minus(),
                (Zero, Minus)   => Signs::plus(),
                (Zero, Zero)    => Signs::zero(),
                (Zero, Plus)    => Signs::minus(),
                (Plus, Minus)   => Signs::plus(),
                (Plus, Zero)    => Signs::plus(),
                (Plus, Plus)    => Signs::all(),
            }
        },
        BinOp::Times => {
            match (left, right) {
                (Minus, Minus)  => Signs::plus(),
                (Minus, Zero)   => Signs::zero(),
                (Minus, Plus)   => Signs::minus(),
                (Zero, Minus)   => Signs::zero(),
                (Zero, Zero)    => Signs::zero(),
                (Zero, Plus)    => Signs::zero(),
                (Plus, Minus)   => Signs::minus(),
                (Plus, Zero)    => Signs::zero(),
                (Plus, Plus)    => Signs::plus(),
            }
        },
        BinOp::Div => {
            match (left, right) {
                (Minus, Minus)  => Signs::plus(),
                (Minus, Zero)   => Signs::new(), // ???
                (Minus, Plus)   => Signs::minus(),
                (Zero, Minus)   => Signs::zero(),
                (Zero, Zero)    => Signs::new(), // ???
                (Zero, Plus)    => Signs::zero(),
                (Plus, Minus)   => Signs::minus(),
                (Plus, Zero)    => Signs::new(), // ???
                (Plus, Plus)    => Signs::plus(),
            }
        },
        BinOp::Rem => {
            match (left, right) {
                (Minus, Minus)  => Signs::zero() + Signs::minus(),
                (Minus, Zero)   => Signs::new(), // ???
                (Minus, Plus)   => Signs::zero() + Signs::plus(),
                (Zero, Minus)   => Signs::zero(),
                (Zero, Zero)    => Signs::new(), // ???
                (Zero, Plus)    => Signs::zero(),
                (Plus, Minus)   => Signs::zero() + Signs::minus(),
                (Plus, Zero)    => Signs::new(), // ???
                (Plus, Plus)    => Signs::zero() + Signs::plus(),
            }
        },
        _ => panic!("Non-arithmetic operator in 'arith_signs'"),
    }
}

fn single_expr_signs(env: &SdState, expr: &TExpr) -> Signs {
    use ast::TExprKind::*;
    
    match expr.kind {
        Record(_, _) => unreachable!(),
        Int(value) => {
            if value < 0 {
                Signs::minus()
            } else if value == 0 {
                Signs::zero()
            } else {
                Signs::plus()
            }
        },
        Access(ref loc) => {
            let mut bloc = BasicLoc::Int(usize::MAX);
            visit_loc_locs(loc, |loc| {
                bloc = loc;
            });
            assert!(bloc != BasicLoc::Int(usize::MAX));
            *env.get(&bloc).expect("Variable not declared before use")
        },
        Bool(_) => unreachable!(),
        Binary(ref left, op, ref right) => {
            let l_signs = single_expr_signs(env, left);
            let r_signs = single_expr_signs(env, right);
            let mut signs = Signs::new();
            for &l_sign in l_signs.signs() {
                for &r_sign in r_signs.signs() {
                    signs += arith_signs(l_sign, op, r_sign);
                }
            }
            signs
        },
        Unary(op, ref _expr) => {           
            //let mut signs = single_expr_signs(env, expr);
            match op {
                UnOp::Not => panic!("Found unary not in arithmetic expression"),
                // No negation unary operator yet. Well. Now that's implemented.
                /*UnOp::Neg => {
                    match (signs.has_plus(), signs.has_minus()) {
                        (true, true) => {}
                        (false, false) => {}
                        (true, false) => {
                            signs += Signs::minus();
                            signs -= Signs::plus();
                        }
                        (false, true) => {
                            signs += Signs::plus();
                            signs -= Signs::minus();
                        }
                    }
                }*/
            }
        },
    }
}

fn expr_signs(env: &SdState, expr: &TExpr) -> ExprSigns {
    use ast::TExprKind::*;
    
    match expr.kind {
        Record(ref first, ref second) => {
            ExprSigns::Record(single_expr_signs(env, first), single_expr_signs(env, second))
        }
        _ => {
            ExprSigns::Integer(single_expr_signs(env, expr))
        }
    }
}

/// Updates signs of values held in basic locations (restricting them).
/// 
/// This is mainly done by branch conditionals, since these restrict the value
/// within the conditional scopes. Also declarations.
///
/// The closure is called with an updated constraint: 'x' = {0,+}
fn update_signs<F: FnMut(BasicLoc, Signs)>(env: &SdState, action: &BasicAction, mut update: F) {
    use analysis::BasicAction::*;
    
    match action {
        Declaration(ref dec) => {
            // Remove the signs of the previous declaration location
            // NOTE: there cannot be a previous declaration yet, so that's good.
            eprintln!("    Updating signs of declaration");
            visit_dec_locs(dec, |loc| {
                update(loc, Signs::zero());
            });
        },
        Test(ref expr) => {
            eprintln!("    Updating signs of test");
            let constraints = bool_constraints(env, expr);
            eprintln!("    Test constraints:");
            eprintln!("    {:?}", constraints);
            for (&loc, &signs) in constraints.iter() {
                update(loc, signs);
            }
        },
        Assignment(ref loc, ref expr) => {
            eprintln!("    Updating signs of assignment");
            let expr_signs = expr_signs(env, expr);
            match expr_signs {
                ExprSigns::Record(first, second) => {
                    let mut offset = 0;
                    visit_loc_locs(loc, |loc| {
                        if offset == 0 {
                            update(loc, first);
                        } else {
                            update(loc, second);
                        }
                        offset += 1;
                    });
                }
                ExprSigns::Integer(value) => {
                    visit_loc_locs(loc, |loc| {
                        update(loc, value);
                    });
                }
            }
        }
        Read(ref loc) => {
            eprintln!("    Updating signs of read");
            visit_loc_locs(loc, |loc| {
                update(loc, Signs::all());
            });
        }
        Write(ref _expr) => {},
        Skip(ref _name) => {},
    }
}


pub type SdState = HashMap<BasicLoc, Signs>;

pub struct SignDetection;

impl SignDetection {
    pub fn new() -> SignDetection {
        SignDetection {}
    }
}

impl Analysis for SignDetection {
    /// The information that this algorithm collects for each point in the
    /// program, such as the set of reaching definitions, live variables,
    /// faint variables etc.
    type State = SdState;
    
    /// Returns the 'empty' information state for each state in the domain.
    /// 
    /// Such as "No definitions reach this point" => Ø.
    fn empty_state() -> Self::State {
        HashMap::new()
    }
    
    /// How the first state should be initialized by default.
    fn initial_state(_start_id: VertexId, _program: &Prograph) -> Self::State {
        // Undefined signs in all locations
        HashMap::new()
    }
    
    /// Whether this analysis traverses the program graph in forward or 
    /// backward direction (opposite the edge direction).
    fn is_forward_analysis() -> bool {
        true
    }
    
    /// Transfers information from one state to the next through an edge, and
    /// returns whether this changed the destination state.
    /// 
    /// This is where the items based on the Kill and Gen sets for the
    /// bit-vector framework are transferred.
    fn transfer<'a>(&mut self, src: &mut Self::State, edge: &ProgEdge<'a>, dst: &mut Self::State, is_first_visit: bool, program: &Prograph) -> bool {
        let mut updated = is_first_visit;
        
        // Transfer the whole state the first time.
        if is_first_visit {
            *dst = src.clone();
        
        // Combine the new possible signs with the old ones
        } else {
            for (&loc, &signs) in src.iter() {
                eprintln!("  Pending union: {}: {}", program.name(loc), signs);
                
                if let Some(&prev_signs) = dst.get(&loc) {
                    let combined = prev_signs + signs;
                    if combined != prev_signs {
                        eprintln!("    Updated {}: {} => {}", program.name(loc), prev_signs, combined);
                        dst.insert(loc, combined);
                        updated = true;
                    }
                } else {
                    eprintln!("    Added {}: {}", program.name(loc), signs);
                    dst.insert(loc, signs);
                    updated = true;
                };
            }
        }
        
        // I cannot classify this reliably in any simple way, so now it will not be.
        let is_chokepoint = false;
        
        // Chokepoint: Updates overwrite
        if is_chokepoint {
            unimplemented!();
        
        // Non-chokepoint: Updates are merged
        } else {
            update_signs(src, &edge.label, |loc, signs| {
                eprintln!("  Pending update: {}: {}", program.name(loc), signs);
                
                if let Some(&prev_signs) = dst.get(&loc) {
                    let combined = prev_signs + signs;
                    if combined != prev_signs {
                        eprintln!("    Updated {}: {} => {}", program.name(loc), prev_signs, combined);
                        dst.insert(loc, combined);
                        updated = true;
                    }
                } else {
                    eprintln!("    Added {}: {}", program.name(loc), signs);
                    dst.insert(loc, signs);
                    updated = true;
                };
            });
        }
        
        updated
    }

}

