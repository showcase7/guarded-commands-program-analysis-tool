//! Module for program analysis functionality.

pub mod general;
pub mod prograph;
pub mod reachdefs;
pub mod livevars;
pub mod faintvars;
pub mod signdet;

pub use analysis::general::{Analysis, WorkList, StackWl, QueueWl, RevPostWl, RoundRobinWl};
pub use analysis::prograph::{Prograph, ProgEdge, BasicAction, BasicLoc, EdgeId, VertexId};
pub use analysis::reachdefs::{RdState, ReachingDefinitions};
pub use analysis::livevars::{LiveVariables, LvState};
pub use analysis::faintvars::{FaintVariables, FvState};
pub use analysis::signdet::{SignDetection, SdState};
