//! Module with the instruction set supported by the virtual machine.

/// Identifies a label, which is a place where branch instructions can jump to.
pub type LabelId = usize;

/// A virtual instruction for the virtual machine.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Instr {
    //  ======================= Stack manipulation  ======================= 
    
    /// Pushes the given value on top of the stack
    Push(i64),
    
    /// Duplicates the top value on the stack
    Dup,
    
    /// Pops the top value and pushes the value found at this stack address.
    Load,
    
    /// Swaps the two top values of the stack.
    Swap,
    
    /// Pops the top value and stores it at the address of the new top value.
    Store,
    
    // Pops the top value of the stack.
    Pop,
    
    //  ======================= Arithmetic ======================= 
    /// Pops the top two values on the stack and pushes `next + top`
    Add,
    
    /// Pops the top two values on the stack and pushes `next - top`
    Sub,
    
    /// Pops the top two values on the stack and pushes `next * top`
    Mul,
    
    /// Pops the top two values on the stack and pushes `next / top`
    Div,
    
    /// Pops the top two values on the stack and pushes `next % top`
    Rem,
    
    //  ======================= Arithmetic immediate ======================= 
    //AddI(i64),
    //SubI(i64),
    //MulI(i64),
    //DivI(i64),
    //RemI(i64),
    
    //  ======================= Logical ======================= 
    
    /// Pops the two top values and pushes `1 if next < top or 0 otherwise`
    Lt,
    
    /// Pops the two top values and pushes `1 if next > top or 0 otherwise`
    Gt,
    
    /// Pops the two top values and pushes `1 if next == top or 0 otherwise`
    Eq,
    
    /// Pops the top value and pushes `1 if it is 0 or 0 otherwise`
    Not,
    
    /// Pops the two top values and pushes `1 if they are both not 0 or 0 otherwise`
    And,
    
    /// Pops the two top values and pushes `1 if either is not 0 or 0 otherwise`
    Or,
    
    // ======================= Flow control ======================= 
    
    /// Jumps unconditionally to the given instruction number
    Jump(usize),
    
    /// Pops the top stack value and jumps to the given instruction number
    /// if the value is not zero.
    JumpIfNotZero(usize),
    
    // ===================== Symbolic flow control ==================
    /// Marks a position as a jump target. Replaced by a skip when the
    /// instructions are compiled.
    Label(LabelId),
    
    /// Jumps unconditionally to the given label.
    JumpToLabel(LabelId),
    
    /// Pops the top stack value and jumps to the given label if the value is 
    /// not zero.
    JumpToLabelIfNotZero(LabelId),
    
    
    //  ================= Syscall / Env / Lang-specific ====================
    
    /// Pops the top stack value and prints it to stdout.
    Write, 
    
    /// Reads an integer value from stdin and places it on the stack.
    Read, 
    
    /// Does nothing.
    Skip,
    
    /// Aborts the program with the given status code.
    Exit(i64),
    
}







