//! Module to check that a program is syntactically and semantically correct,
//! and transform it for later compiler passes.

use ast::{self, *};
use std::collections::HashMap;
use std::ops::Deref;
use span::Span;

/// An syntactic or semantic error found in a program.
#[derive(Debug, Clone, PartialEq)]
pub enum TypeError {
    /// The given variable was referred to but wasn't defined.
    UnboundVariable(Ident),
    
    //NameBoundTwice { first: Ident, second: Ident },
    /// The program contains no statements.
    NoStatementsInProgram,
    
    /// An array was indexed using a wrong type of expression.
    InvalidIndexType { array: Ident, value: Expr },
    
    /// Something that isn't an array was indexed.
    LocationNotAnArray { ident: Ident, typ: Type },
    
    /// Something that isn't a record had a record expression assigned.
    LocationNotARecord { ident: Ident, typ: Type },
    
    /// A variable containing a record was used as an access.
    RecordVarAsExpression(Expr),
    
    /// A variable containing an integer array was assigned to.
    IntArrayAsLocation(Loc),
    
    /// A value with a different type was being assigned to a location.
    InvalidAssignment { location: TLoc, value: TExpr },
    
    /// A condition wasn't a boolean value.
    NonBooleanCondition(Expr),
    
    /// A location that isn't an integer was the destination of a read statement.
    ReadLocationNotInteger(TLoc),
    
    /// An expression was expected to be an integer.
    NotAnInteger(TExpr),
    
    /// An expression was expected to be a boolean.
    NotABoolean(TExpr),
    
    /// The left argument to a binary operator had a wrong type.
    InvalidLeftArgument {
        argument: TExpr,
        op: BinOp,
        expected: Type,
        full_span: Span,
    },
    
    /// The right argument to a binary operator had a wrong type.
    InvalidRightArgument {
        argument: TExpr,
        op: BinOp,
        expected: Type,
        full_span: Span,
    },
}

struct Env {
    /// Maps from a variable name the type and bound id for the variable with
    /// that name in the current scope.
    vars: HashMap<String, (Type, VarId)>,
    
    /// Errors found when type-checking.
    errors: Vec<TypeError>,
    
    /// A map from variable ids to their names.
    var_names: Vec<String>,
    // no local scope yet.
}

impl Env {
    pub fn new() -> Env {
        Env {
            vars: HashMap::new(),
            errors: Vec::new(),
            var_names: Vec::new(),
        }
    }
    
    fn bind(&mut self, dec: &Dec) -> VarId {
        let var_id = self.var_names.len();
        self.var_names.push(dec.var());
        self.vars.insert(dec.var(), (dec.typ(), var_id));
        var_id
    }
    
    fn look_up(&mut self, ident: &Ident) -> Option<(Type, VarId)> {
        if let Some(&(typ, var_id)) = self.vars.get(&ident.text) {
            Some((typ, var_id))
        } else {
            self.errors.push(TypeError::UnboundVariable(ident.clone()));
            None
        }
    }
    
    fn add_error(&mut self, error: TypeError) {
        self.errors.push(error);
    }
}

fn type_check_expression(expr: &Expr, env: &mut Env) -> Option<TExpr> {
    use ast::ExprKind::*;
    
    match expr.kind {
        Int(value) => {
            Some(TExpr {
                kind: TExprKind::Int(value),
                typ: Type::Int,
                span: expr.span,
            })
        }
        Access(ref loc) => {
            let tloc = type_check_location(loc, env)?;
            let typ = tloc.typ;
            Some(TExpr {
                kind: TExprKind::Access(tloc),
                typ: typ,
                span: expr.span,
            })
        }
        Bool(value) => {
            Some(TExpr {
                kind: TExprKind::Bool(value),
                typ: Type::Bool,
                span: expr.span,
            })
        }
        Unary(op, ref value) => {
            let texpr = type_check_expression(value, env)?;
            match (op, texpr.typ) {
                (UnOp::Not, Type::Bool) => {
                    Some(TExpr {
                        kind: TExprKind::Unary(op, Box::new(texpr)),
                        typ: Type::Bool,
                        span: expr.span,
                    })
                }
                (UnOp::Not, _) => {
                    env.add_error(TypeError::NotABoolean(texpr));
                    None
                }
            }
        }
        Binary(ref left, op, ref right) => {
            use ast::BinOp::*;
            
            let t_left = type_check_expression(left, env);
            let t_right = type_check_expression(right, env);
            let (arg_type, ret_type) = match op {
                Plus | Minus | Times | Div | Rem => {
                    (Type::Int, Type::Int)
                }
                Lt | LtEq | Gt | GtEq | Eq | NotEq => {
                    (Type::Int, Type::Bool)
                }
                And | Or => {
                    (Type::Bool, Type::Bool)
                }
            };
            
            // Check that the left argument has the correct type.
            let valid_left = if let Some(ref left) = t_left {
                if left.typ == arg_type {
                    true
                } else {
                    env.add_error(TypeError::InvalidLeftArgument {
                        argument: left.clone(),
                        op: op,
                        expected: arg_type,
                        full_span: expr.span,
                    });
                    false
                }
            } else {
                false
            };
            
            // Check that the right argument has the correct type.
            let valid_right = if let Some(ref right) = t_right {
                if right.typ == arg_type {
                    true
                } else {
                    env.add_error(TypeError::InvalidRightArgument {
                        argument: right.clone(),
                        op: op,
                        expected: arg_type,
                        full_span: expr.span,
                    });
                    false
                }
            } else {
                false
            };
            
            if valid_left && valid_right {
                Some(TExpr {
                    kind: TExprKind::Binary(
                        Box::new(t_left.unwrap()), 
                        op, 
                        Box::new(t_right.unwrap())
                    ),
                    typ: ret_type,
                    span: expr.span,
                })
            } else {
                None
            }
        }
        Record(ref fst, ref snd) => {
            let t_fst = type_check_expression(fst, env);
            let t_snd = type_check_expression(snd, env);
            
            // Ensure that the first value is an int.
            let valid_fst = if let Some(ref fst) = t_fst {
                if fst.typ != Type::Int {
                    env.add_error(TypeError::NotAnInteger(fst.clone()));
                    false
                } else {
                    true
                }
            } else {
                false
            };
            
            // Ensure that the second value is an int.
            let valid_snd = if let Some(ref snd) = t_snd {
                if snd.typ != Type::Int {
                    env.add_error(TypeError::NotAnInteger(snd.clone()));
                    false
                } else {
                    true
                }
            } else {
                false
            };
            
            if valid_fst && valid_snd {
                Some(TExpr {
                    kind: TExprKind::Record(
                        Box::new(t_fst.unwrap()), 
                        Box::new(t_snd.unwrap())
                    ),
                    typ: Type::RecordExpr,
                    span: expr.span,
                })
            } else {
                None
            }
        }
    }
}

fn type_check_location(loc: &Loc, env: &mut Env) -> Option<TLoc> {
    use ast::LocKind::*;
    
    match loc.kind {
        Var => {
            // Check var bound
            let (typ, var_id) = env.look_up(&loc.name)?;
            Some(TLoc {
                var: ast::Var { id: var_id, span: loc.name.span },
                kind: TLocKind::Var,
                span: loc.span.clone(),
                typ: typ,
            })
        }
        Arr(ref expr) => {
            // Check that this is an array
            let arr_info = env.look_up(&loc.name);
            let var_info = match arr_info {
                Some((Type::IntArr(_), var_id)) => {
                    Some((Type::Int, var_id))
                }
                Some((other_typ, _)) => {
                    env.add_error(TypeError::LocationNotAnArray {
                        ident: loc.name.clone(),
                        typ: other_typ,
                    });
                    None
                }
                _ => None,
            };
            
            // Check that the index expression is an integer.
            let texpr = type_check_expression(expr, env);
            if let Some(ref texpr) = texpr {
                if texpr.typ != Type::Int {
                    env.add_error(TypeError::InvalidIndexType {
                        array: loc.name.clone(),
                        value: expr.deref().clone(),
                    });
                }
            }
            
            if let (Some((typ, var_id)), Some(texpr)) = (var_info, texpr) {
                Some(TLoc {
                    var: ast::Var { id: var_id, span: loc.name.span },
                    span: loc.span.clone(),
                    typ: typ,
                    kind: TLocKind::Arr(Box::new(texpr)),
                })
            } else {
                None
            }
        }
        Field(ref field_name) => {
            // Check that this is a record
            let rec_info = env.look_up(&loc.name);
            match rec_info {
                Some((Type::Record, var_id)) => {
                    let field_number = match field_name.text.as_str() {
                        "fst" => 0,
                        "snd" => 1,
                        _ => unreachable!(),
                    };
                    Some(TLoc {
                        var: ast::Var { id: var_id, span: loc.name.span },
                        span: loc.span.clone(),
                        // TODO: new field types
                        typ: Type::Int,
                        kind: TLocKind::Field(field_number),
                    })
                }
                Some((other_typ, _)) => {
                    env.add_error(TypeError::LocationNotARecord {
                        ident: loc.name.clone(),
                        typ: other_typ,
                    });
                    None
                }
                _ => None,
            }
        }
    }
}

fn type_check_statement(stmt: &Stmt, env: &mut Env) -> Option<TStmt> {
    use ast::StmtKind::*;
    
    match stmt.kind {
        Assign(ref loc, ref value) => {
            let tloc = type_check_location(loc, env);            
            let tval = type_check_expression(value, env);
            
            if let (Some(tloc), Some(tval)) = (tloc, tval) {
                
                // Integer arrays cannot be assigned to.
                if let Type::IntArr(_) = tloc.typ {
                    env.add_error(TypeError::IntArrayAsLocation(
                        loc.clone()
                    ));
                    return None;
                }
                
                // Record references are not valid expressions.
                if let Type::Record = tval.typ {
                    env.add_error(TypeError::RecordVarAsExpression(
                        value.deref().clone()
                    ));
                    return None;
                }
                
                
                // Ensure that the assigned value matches the location.
                match (tloc.typ, tval.typ) {
                    // loc should not admit bool
                    (Type::Bool, _) => unreachable!(),
                    
                    (Type::Record, Type::RecordExpr) |
                    (Type::Int, Type::Int) => {
                        Some(TStmt {
                            kind: TStmtKind::Assign(tloc, tval),
                            span: stmt.span,
                        })
                    }
                    _ => {
                        env.add_error(TypeError::InvalidAssignment {
                            location: tloc,
                            value: tval,
                        });
                        None
                    }
                }
            } else {
                None
            }
        },
        If(ref cond, ref body, ref else_body) => {
            // Ensure that the condition is a boolean.
            let tcond = type_check_expression(cond, env);
            if let Some(ref expr) = tcond {
                if expr.typ != Type::Bool {
                    env.add_error(TypeError::NonBooleanCondition(
                        cond.deref().clone()
                    ));
                }
            }
            
            // Ensure that all the body statements are well-typed.
            let mut had_error = false;
            let mut typed_body = Vec::new();
            for stmt in body {
                if let Some(stmt) = type_check_statement(stmt, env) {
                    typed_body.push(stmt);
                } else {
                    had_error = true;
                }
            }
            
            // Ensure that all the else body statements are well-typed.
            let typed_else = if let Some(ref else_body) = else_body {
                let mut typed_else_body = Vec::new();
                for stmt in else_body {
                    if let Some(stmt) = type_check_statement(stmt, env) {
                        typed_else_body.push(stmt);
                    } else {
                        had_error = true;
                    }
                }
                Some(typed_else_body)
            } else {
                None
            };
            
            if had_error {
                return None;
            }
            
            if let Some(cond) = tcond {
                Some(TStmt {
                    kind: TStmtKind::If(cond, typed_body, typed_else),
                    span: stmt.span,
                })
            } else {
                None
            }
        },
        While(ref cond, ref body) => {
            // Ensure that the condition is a boolean.
            let tcond = type_check_expression(cond, env);
            if let Some(ref expr) = tcond {
                if expr.typ != Type::Bool {
                    env.add_error(TypeError::NonBooleanCondition(
                        cond.deref().clone()
                    ));
                }
            }
            
            // Ensure that all the body statements are well-typed.
            let mut had_error = false;
            let mut typed_body = Vec::new();
            for stmt in body {
                if let Some(stmt) = type_check_statement(stmt, env) {
                    typed_body.push(stmt);
                } else {
                    had_error = true;
                }
            }
            
            if had_error {
                return None;
            }
            
            if let Some(cond) = tcond {
                Some(TStmt {
                    kind: TStmtKind::While(cond, typed_body),
                    span: stmt.span,
                })
            } else {
                None
            }
        },
        Break => {
            Some(TStmt { 
                kind: TStmtKind::Break, 
                span: stmt.span 
            })
        },
        Read(ref loc) => {
            let tloc = type_check_location(loc, env)?;
            if tloc.typ != Type::Int {
                env.add_error(TypeError::ReadLocationNotInteger(tloc));
                None
            } else {
                Some(TStmt {
                    kind: TStmtKind::Read(tloc),
                    span: stmt.span,
                })
            }
        },
        Write(ref value) => {
            let texpr = type_check_expression(value, env)?;
            if texpr.typ != Type::Int {
                env.add_error(TypeError::NotAnInteger(texpr));
                None
            } else {
                Some(TStmt {
                    kind: TStmtKind::Write(texpr),
                    span: stmt.span,
                })
            }
        },
    }
}

/// Ensures that the given program is syntactically correct and that it isn't
/// semantically incorrect according to the current set of checks.
/// 
/// Also converts the program to a typed Abstract Syntax Tree in which variables
/// have been bound to ids.
pub fn type_check(program: &Program) -> Result<TProgram, Vec<TypeError>> {
    let mut statements = Vec::new();
    let mut env = Env::new();
    let mut decs = Vec::new();
    
    if program.statements.is_empty() {
        env.errors.push(TypeError::NoStatementsInProgram);
    }
    
    for dec in &program.declarations {
        let var_id = env.bind(dec);
        let tdec = TDec {
            var: Var {
                id: var_id,
                span: dec.name.span,
            },
            kind: match dec.kind {
                DecKind::Int => TDecKind::Int,
                DecKind::IntArr(size) => TDecKind::IntArr(size),
                DecKind::Record => TDecKind::Record,
            },
            span: dec.span,
        };
        decs.push(tdec);
    }
    
    for stmt in &program.statements {
        if let Some(tstmt) = type_check_statement(&stmt, &mut env) {
            statements.push(tstmt);
        }
    }
    
    if ! env.errors.is_empty() {
        Err(env.errors)
    } else {
        Ok(TProgram {
            declarations: decs,
            statements: statements,
            var_names: env.var_names,
        })
    }
}


