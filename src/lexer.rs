//! Module to convert a source file into a stream of syntactical elements 
//! called tokens.

use std::str::CharIndices;
use std::iter::Peekable;

use span::Span;

// ========================= Tokens ==================================

/// A single syntactical token of the MicroC language.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Token {
    /// What kind of token this is.
    pub kind: TokenKind,
    
    /// Where it came from.
    pub span: Span,
}

impl Token {
    /// Creates a new token found at the given position.
    pub fn new(kind: TokenKind, start: usize, end: usize) -> Token {
        Token {
            kind,
            span: Span {
                start,
                end,
            }
        }
    }
}

/// The set of keywords in the MicroC language.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Kw {
    // Types
    Int,
    
    // Declaration stuff?
    Fst, Snd,
    
    // Constants
    True, False,
    
    // Ops / builtins
    Not, Write, Read,
    
    // Flow control
    While, If, Else,
    Break,
}

/// Different types of tokens.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TokenKind {
    Ident,
    Integer,
    Keyword(Kw),
    
    // 'Ignored' tokens
    Comment,
    Whitespace,
    
    // Markers
    ParOpen, ParClose,
    BrackOpen, BrackClose,
    CurlyOpen, CurlyClose,
    
    Dot, Comma, Semicolon,
    
    ColonEq,
    
    // Arithmetic operators
    Plus, Minus, Star, Slash, Percent,
    
    // Equality operators
    Lt, Gt, LtEq, GtEq, Eq, NotEq,
    
    // boolean operators
    Ampersand, VLine,
    
    // End of stream marker to simplify parsing a lot
    Eos,
}

// ========================= Lexers ==================================

/// An error found when lexing a source file.
pub struct LexError<'src> {
    /// The text of the source file.
    pub source: &'src str,
    
    /// The byte position where the error was found.
    pub position: usize,
    
    /// The line where the error was found. (1-indexed).
    pub line: usize,
    
    /// The column where the error was found. (1-indexed).
    pub col: usize,
    
    /// A message describing the error.
    pub message: String,
}

pub type LexResult<'src, T> = Result<T, LexError<'src>>;

/// A lexer that preserves whitespace and comments.
pub struct RawLexer<'src> {
    source: &'src str,
    stream: Peekable<CharIndices<'src>>,
    line: usize,
    line_start: usize,
    finished: bool,
}

impl<'src> RawLexer<'src> {
    pub fn new(source: &'src str) -> RawLexer<'src> {
        RawLexer {
            source,
            stream: source.char_indices().peekable(),
            line: 1,
            line_start: 0,
            finished: false,
        }
    }
    
    pub fn cur_pos(&mut self) -> usize {
        if let Some(&(i, _)) = self.stream.peek() {
            i
        } else {
            self.source.len()
        }
    }
    
    #[inline]
    fn wrap(&self, kind: TokenKind, start: usize, end: usize) -> LexResult<'src, Token> {
        Ok(Token::new(kind, start, end))
    }
    
    fn next_token(&mut self) -> Option<LexResult<'src, Token>> {
        use self::TokenKind::*;
        
        // Return None if we're out of characters.
        let (start, ch) = if let Some(token) = self.stream.next() {
            token
        } else {
            if self.finished {
                return None;
            } else {
                self.finished = true;
                return Some(self.wrap(Eos, self.source.len(), self.source.len()));
            }
        };
        
        // Create a nice peek character for two-character patterns.
        let (_, peek) = self.stream.peek().map(|&x| x).unwrap_or((0, ' '));
        
        macro_rules! wrap_1 {
            ($kind:expr) => {
                self.wrap($kind, start, start+1)
            }
        };
        
        macro_rules! wrap_2 {
            ($kind:expr) => {
                {
                    self.stream.next();
                    self.wrap($kind, start, start+2)
                }
            }
        };
        
        Some(match (ch, peek) {
            ('.', _)    => wrap_1!(Dot),
            (',', _)    => wrap_1!(Comma),
            (';', _)    => wrap_1!(Semicolon),
            (':', '=')  => wrap_2!(ColonEq),
            
            ('(', _)    => wrap_1!(ParOpen),
            (')', _)    => wrap_1!(ParClose),
            ('[', _)    => wrap_1!(BrackOpen),
            (']', _)    => wrap_1!(BrackClose),
            ('{', _)    => wrap_1!(CurlyOpen),
            ('}', _)    => wrap_1!(CurlyClose),
            
            ('+', _)    => wrap_1!(Plus),
            ('-', _)    => wrap_1!(Minus),
            ('*', _)    => wrap_1!(Star),
            ('%', _)    => wrap_1!(Percent),
            
            ('&', _)    => wrap_1!(Ampersand),
            ('|', _)    => wrap_1!(VLine),
            
            ('<', '=')  => wrap_2!(LtEq),
            ('<', _)    => wrap_1!(Lt),
            ('>', '=')  => wrap_2!(GtEq),
            ('>', _)    => wrap_1!(Gt),
            ('=', '=')  => wrap_2!(Eq),
            ('!', '=')  => wrap_2!(NotEq),
            
            // ========================== Integers ============================
            
            ('0'...'9', _) => {
                while let Some(&(i, ch)) = self.stream.peek() {
                    match ch {
                        '0'...'9' => {
                            self.stream.next();
                        }
                        _ => {
                            return Some(self.wrap(Integer, start, i));
                        }
                    }
                }
                self.wrap(Integer, start, self.source.len())
            }
            
            // ==================== Identifier or keyword =====================
            
            ('a'...'z', _) |
            ('A'...'Z', _) |
            ('_', _) => {
                while let Some(&(i, ch)) = self.stream.peek() {
                    match ch {
                        'a'...'z' |
                        'A'...'Z' |
                        '0'...'9' |
                        '_' => {
                            self.stream.next();
                        }
                        _ => {
                            // Find out whether this is an ident or keyword
                            let text = &self.source[start..i];
                            macro_rules! wrap_kw {
                                ($kw:expr) => {
                                    self.wrap(Keyword($kw), start, i)
                                }
                            };
                            return Some(match text {
                                "int"   => wrap_kw!(Kw::Int),
                                "true"  => wrap_kw!(Kw::True),
                                "false" => wrap_kw!(Kw::False),
                                
                                "not"   => wrap_kw!(Kw::Not),
                                "read"  => wrap_kw!(Kw::Read),
                                "write" => wrap_kw!(Kw::Write),
                                
                                "fst"   => wrap_kw!(Kw::Fst),
                                "snd"   => wrap_kw!(Kw::Snd),
                                
                                "while" => wrap_kw!(Kw::While),
                                "if"    => wrap_kw!(Kw::If),
                                "else"  => wrap_kw!(Kw::Else),
                                "break" => wrap_kw!(Kw::Break),
                                
                                _ => {
                                    self.wrap(Ident, start, i)
                                }
                            });
                            
                        }
                    }
                }
                self.wrap(Ident, start, self.source.len())
            }
            
            // ===================== Comments ========================
            
            ('/', '/') => {
                while let Some(&(i, ch)) = self.stream.peek() {
                    if ch == '\n' {
                        return Some(self.wrap(Comment, start, i));
                    } else {
                        self.stream.next();
                    }
                }
                self.wrap(Comment, start, self.source.len())
            }
            
            ('/', '*') => {
                self.stream.next();
                while let Some((i, ch)) = self.stream.next() {
                    let (_, peek) = self.stream.peek().map(|&x| x).unwrap_or((0, ' '));
                    if let ('*', '/') = (ch, peek) {
                        self.stream.next();
                        return Some(self.wrap(Comment, start, i));
                    }
                }
                let col = &self.source[self.line_start..start].chars().count() + 1;
                Err(LexError {
                    source: self.source,
                    position: start,
                    line: self.line,
                    col: col,
                    message: format!("Unclosed comment"),
                })
            }
            
            ('/', _) => wrap_1!(Slash),
            
            // =================== Whitespace ======================
            
            (ch, _) if ch.is_whitespace() => {
                while let Some(&(i, ch)) = self.stream.peek() {
                    if ch == '\n' {
                        self.line += 1;
                        self.line_start = i + 1;
                        self.stream.next();
                    
                    } else if ch.is_whitespace() {
                        self.stream.next();
                    
                    } else {
                        return Some(self.wrap(Whitespace, start, i));
                    }
                }
                self.wrap(Whitespace, start, self.source.len())
            }
            
            // ==================== Default =====================
            
            _ => {
                let col = &self.source[self.line_start..start].chars().count() + 1;
                Err(LexError {
                    source: self.source,
                    position: start,
                    line: self.line,
                    col: col,
                    message: format!("Unexpected character '{}'", ch),
                })
            }
        })
    }
}

impl<'src> Iterator for RawLexer<'src> {
    type Item = LexResult<'src, Token>;
    
    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }
}

/// A lexer which filters out comments and whitespace.
pub struct Lexer<'src> {
    lexer: RawLexer<'src>,
}

impl<'src> Lexer<'src> {
    /// Creates a new filtering lexer to lex the given source file text.
    pub fn new(source: &'src str) -> Lexer<'src> {
        Lexer {
            lexer: RawLexer::new(source),
        }
    }
    
    #[inline]
    pub fn cur_pos(&mut self) -> usize {
        self.lexer.cur_pos()
    }
}

impl<'src> Iterator for Lexer<'src> {
    type Item = LexResult<'src, Token>;
    
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(res) = self.lexer.next() {
            match res {
                Ok(token) => {
                    match token.kind {
                        TokenKind::Whitespace |
                        TokenKind::Comment => {
                            continue;
                        }
                        _ => {
                            return Some(Ok(token));
                        }
                    }
                }
                Err(err) => {
                    return Some(Err(err));
                }
            }
        }
        None
    }
}
