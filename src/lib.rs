
pub mod span;
pub mod ast;

pub mod lexer;
pub mod parser;
pub mod typeck;

pub mod format;

pub mod instructions;
pub mod codegen;
pub mod vm;

pub mod analysis;

#[cfg(test)]
mod test;

pub use span::{Span, text_pos};
pub use lexer::{Lexer, Token, TokenKind, Kw, LexError};
pub use parser::{parse, Parser, ParseError, ParseErrorKind};
pub use typeck::{type_check, TypeError};
pub use format::format_program_into;
pub use instructions::{Instr, LabelId};
pub use codegen::generate_instructions;
pub use vm::{execute};
pub use vm::status;
pub use analysis::{Prograph, BasicLoc, BasicAction};
