//! Module with types to describe an untyped and typed Abstract Syntax Tree.

use span::Span;

// ======================== Abstract Syntax Tree ===============================

/// Binary (double-argument) operators.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BinOp {
    Plus,
    Minus,
    Times,
    Div,
    Rem,
    // ...
    Lt,
    LtEq,
    Gt,
    GtEq,
    Eq,
    NotEq,
    // ...
    And,
    Or,
}

/// Unary (single-argument) operators.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum UnOp {
    Not,
}

/// An untyped identifier, that is, a string used to identify a variable, type,
/// field etc..
#[derive(Debug, Clone, PartialEq)]
pub struct Ident {
    /// The name that this describes.
    pub text: String,
    
    /// Where it came from.
    pub span: Span,
}

/// Different declaration types.
#[derive(Debug, Clone, PartialEq)]
pub enum DecKind {
    /// Declare an integer.
    Int,
    
    /// Declare an integer array with a fixed size.
    IntArr(usize),
    
    /// Declare a record.
    Record,
}

/// Describes a declaration of a new variable of a given type.
#[derive(Debug, Clone, PartialEq)]
pub struct Dec {
    /// The variable name.
    pub name: Ident,
    
    /// What kind of declaration this is.
    pub kind: DecKind,
    
    /// Where it came from.
    pub span: Span,
}

impl Dec {
    /// Returns the type of this declaration.
    pub fn typ(&self) -> Type {
        use ast::DecKind::*;
        match self.kind {
            Int => Type::Int,
            IntArr(size) => Type::IntArr(size),
            Record => Type::Record,
        }
    }
    
    /// Returns the variable name of this declaration.
    pub fn var(&self) -> String {
        self.name.text.clone()
    }
}

/// Different location types.
#[derive(Debug, Clone, PartialEq)]
pub enum LocKind {
    /// A variable.
    Var,
    
    /// Inside an array, at the index described by the expression.
    Arr(Box<Expr>),
    
    /// Inside a record, at the field referred to by the identifier.
    Field(Ident),
}

/// A location describes the 'address' of a type, and can be assigned to using
/// assignment and read statements, or read from using access expressions.
#[derive(Debug, Clone, PartialEq)]
pub struct Loc {
    /// The variable of this location.
    pub name: Ident,
    
    /// What kind of location it is.
    pub kind: LocKind,
    
    /// Where it came from.
    pub span: Span,
}

/// Different expression types.
#[derive(Debug, Clone, PartialEq)]
pub enum ExprKind {
    /// An integer literal.
    Int(i64),
    
    /// A value stored somewhere.
    Access(Loc),
    
    /// A boolean literal.
    Bool(bool),
    
    /// The application of a binary operator to two expressions.
    Binary(Box<Expr>, BinOp, Box<Expr>),
    
    /// The application of a unary operator to a single expression.
    Unary(UnOp, Box<Expr>),
    
    /// A record value literal. (Assigned to a record-type variable).
    Record(Box<Expr>, Box<Expr>),
}

/// An expression; an action that has or generates a value.
#[derive(Debug, Clone, PartialEq)]
pub struct Expr {
    /// What kind of expression this is.
    pub kind: ExprKind,
    
    /// Where it came from.
    pub span: Span,
}

/// Different types of statements.
#[derive(Debug, Clone, PartialEq)]
pub enum StmtKind {
    /// Assigns a value to a memory location.
    Assign(Loc, Expr),
    
    /// Takes one path if the condition is true and another if it isn't.
    If(Expr, Vec<Stmt>, Option<Vec<Stmt>>),
    
    /// Checks whether the condition is true and if so repeats the given sequence
    /// of statements until the condition isn't true after running it.
    While(Expr, Vec<Stmt>),
    
    /// Moves execution to after the while loop it is currently inside.
    Break,
    
    /// Reads an integer value from standard input into the given location.
    Read(Loc),
    
    /// Writes the given integer expression to standard output.
    Write(Expr),
}

/// A statement; one or more nested actions that change the state and flow of 
/// the program.
#[derive(Debug, Clone, PartialEq)]
pub struct Stmt {
    /// What kind of statement this is.
    pub kind: StmtKind,
    
    /// Where it came from.
    pub span: Span,
}

/// A program; a list of variable declarations and a set of statements to run
/// with these variables declared.
#[derive(Debug, Clone, PartialEq)]
pub struct Program {
    /// The variables declared in the program.
    pub declarations: Vec<Dec>,
    
    /// The statements to execute.
    pub statements: Vec<Stmt>,
}

// =================== Typed AST ========================


/// Identifies a variable in the program.
pub type VarId = usize;

/// A bound identifier.
///
/// Basically an identifier that has been replaced by an index to that same
/// identifier.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Var {
    /// Identifies this variable.
    pub id: VarId,
    
    /// Where it came from.
    pub span: Span,
}

/// Types; the kind of values that can appear in the program. Used to ensure
/// that operators and functions are only applied to their expected arguments.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Type {
    /// An integer (a whole number).
    Int,
    
    /// A list containing a fixed number of integers.
    IntArr(usize),
    
    /// A record variable containing two fields 'fst' and 'snd'.
    Record,
    
    /// A boolean (truth) value.
    Bool,
    
    /// An expression that sets all fields of a record.
    RecordExpr,
}

/// Types of typed declarations.
#[derive(Debug, Clone, PartialEq)]
pub enum TDecKind {
    Int,
    IntArr(usize),
    Record,
}

/// A typed declaration.
#[derive(Debug, Clone, PartialEq)]
pub struct TDec {
    pub var: Var,
    pub kind: TDecKind,
    pub span: Span,
}

impl TDec {
    /// Returns the type of the given declaration.
    pub fn typ(&self) -> Type {
        use ast::TDecKind::*;
        match self.kind {
            Int => Type::Int,
            IntArr(size) => Type::IntArr(size),
            Record => Type::Record,
        }
    }
}

/// Types of typed locations.
#[derive(Debug, Clone, PartialEq)]
pub enum TLocKind {
    Var,
    
    // index
    Arr(Box<TExpr>),
    
    /// A field access identified by its index inside the record.
    Field(usize),
}

/// A typed location.
#[derive(Debug, Clone, PartialEq)]
pub struct TLoc {
    pub var: Var,
    pub kind: TLocKind,
    pub span: Span,
    pub typ: Type,
}

/// Types of typed expressions.
#[derive(Debug, Clone, PartialEq)]
pub enum TExprKind {
    Int(i64),
    Access(TLoc),
    Bool(bool),
    Binary(Box<TExpr>, BinOp, Box<TExpr>),
    Unary(UnOp, Box<TExpr>),
    Record(Box<TExpr>, Box<TExpr>),
}

/// A typed expression.
#[derive(Debug, Clone, PartialEq)]
pub struct TExpr {
    pub kind: TExprKind,
    pub typ: Type,
    pub span: Span,
}

/// Types of typed statements.
#[derive(Debug, Clone, PartialEq)]
pub enum TStmtKind {
    Assign(TLoc, TExpr),
    If(TExpr, Vec<TStmt>, Option<Vec<TStmt>>),
    While(TExpr, Vec<TStmt>),
    Break,
    Read(TLoc),
    Write(TExpr),
}

/// A typed statement.
#[derive(Debug, Clone, PartialEq)]
pub struct TStmt {
    pub kind: TStmtKind,
    pub span: Span,
}

/// A typed program; all nodes have a type and identifiers have been bound and
/// are referred to by ids instead. The program is syntactically correct,
/// and is semantically correct according to the type-checking rules.
#[derive(Debug, Clone, PartialEq)]
pub struct TProgram {
    /// The declarations in the program.
    pub declarations: Vec<TDec>,
    
    /// The statements to be executed.
    pub statements: Vec<TStmt>,
    
    /// The names of the variables in the program.
    /// Used to look up printable names of bound variables.
    pub var_names: Vec<String>,
}

